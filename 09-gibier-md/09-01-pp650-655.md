---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->






# ジビエ {#gibier}

<!--<div class="frchapenv">Gibier</div>-->

\nopagebreak


## 猟獣類[^12] {#serie-des-gibiers-a-poil}

<!--<div class="frsecenv">Série de Gibier à Poil</div>-->

フランスで使われる大型ジビエのうちで、赤鹿[^1]、ダマ鹿、イザール[^2]、
シャモワ[^3]などは、[シュヴルイユ\*](#chevreuil-gls)[^5]と同じ分類とな
るから、シュヴルイユと同様の調理をすることになる。だから、これら
については個別に項目を立てる必要はないだろう。

シュヴルイユはマリネはしてもしなくてもいいが、とりわけ若くて肉質の柔ら
かい固体の場合にはマリネしない方がいい。

マリナードの作り方は、[ソースの章](#marinades-et-saumures)で述べたとおりだ。どの程
度マリネするかはもっぱら肉をマリナードに漬けていると時間と、さらに肉質の
柔らかさで調整する。とりわけシュヴルイユの場合にはよく見極める必要があ
る。

[^12]: 初版は「シュヴルイユ」とのみ。第二版から猟獣類として項をあらた
    めているが概説は初版のシュヴルイユのもの。初版と第二版で概説本文の
    表現に若干の異同があるのみ。第二版で現在の文章になり、それ以後の異
    同はない。

[^1]: cerf（セール） 日本鹿、エゾ鹿もこれに含まれる。肉質が固いとしてあ
    まりフランスでは好まれない。


[^2]: isard ピレネー山脈に生息するカモシカの一種。若いものは肉質が柔ら
    かく特に珍重される。


[^3]: アルプス山脈などに生息するカモシカの一種で前出のイザールに非常に
    近い。


[^5]: 体長約1〜1.3m、体重約20〜25kg（成体）。和名ノロ鹿。日本で主に食
されるエゾ鹿（赤鹿類）に比べ小型の別種。ユーラシア大陸 中・高緯度に分布。
夜行性で食性は灌木、草、果実などの植物食。フランスでは年に25万頭ほどを
とる、ポピュラーな猟獣（2000年現在）。生後6ヶ月までをfaon（フォン）、18ヶ
月までをchevrillard（シュヴリヤール）、20〜25kgのオスをbrocard（ブロカー
ル）、メスをchevrette（シュヴレット）またはchèvre（シェーブル）と呼ぶ。




### \hspace*{-1zw}[シュヴルイユ\*](#chevreuil-gls) {#chevreuil}

<!--<div class="frsecbenv">Chevreuil</div>-->

\index{chevreuil@chevreuil|(}
\index{しゆうるいゆ@シュヴルイユ|(}
\index{のろしか@ノロ鹿|see {シュヴルイユ}}


    



<div class="recette"><!--beginRecette-->


#### シュヴルイユのシヴェ[^11] {#civet-de-chevreuil}

<div class="frsubenv">Civet de chevreuil</div>

\index{chevreuil@Chevreuil!civet de ---}
\index{civet@civet!chevreuil@--- de Chevreuil}
\index{しゆうるいゆ@シュヴルイユ!しうえ@シヴェ}
\index{しうえ@シヴェ!しゆうるいゆ@シュヴルイユ}


用いるのは肩肉、首肉、バラ肉、肩寄りの背肉。

それぞれの塊は、シヴェをしっとりさせるために標準的なマリネ液用の香味付けをしたワインで
少なくとも6時間マリネする。このマリナードで煮込むことになる。

シヴェの調理を始める時に、それぞれの塊は水気を切り、拭いてから強火でこんがり表
面を焼く。調理そのものは背肉を使った[リエーヴルのシ
ヴェ](#civet-de-lievre)と同じ。血での[とろみ付け\*](#lier-gls)を省かないこと。そうで
ないとただのラグーになってしまう[^17]。シュヴルイユの場合、血をとっておくの
はまず不可能だから、このとろみ付けにはリエーヴルの血、やむを得ない場合
には繁殖うさぎの血を用いることになる。

[^11]: 語源は ciboule （シブール） は葱にちかい植物で、もともとはシブー
    ルを用いた煮込みだったと考えられているが、中世の料理書ではすでにシ
    ブールを用いないシヴェのレシピが散見される。本書では「血」を用いて
    [リエ\*](#lier-gls)することをシヴェの要件としている。中世以来の料
    理だが、タイユヴァンでは焼いたパンを粉にしてヴェルジュなどで溶いて
    漉してリエしている。このことは、タイユヴァンが野菜の使用を極力控え
    ているように読めることも関係があるだろう。タイユヴァンにおいては空
    に近い鳥類が食材としてのランクが高く、地面のものは低い扱いをしてい
    る。このような世界観は中世料理に共通のものだった。もっとも、タイユ
    ヴァンにおいてもポレ（ポワローやブレットをピュレ状に煮込んだもの）
    など野菜料理が完全に排除されているわけではない。1660年のピエール・
    ドゥ・リュヌ『新料理の本』にも同様のレシピが見られる (p.191)。さて、
    18世紀の『料理小辞典』において血を用いてリエする例が掲載されている
    が、これはリエーヴル・ブルジョワ風のレシピであり、シヴェではない。
    シヴェについては\kenten{別レシピ}として血を用いてリエする方法が掲
    載されている。19世紀初期のヴィアール『帝国料理の本』（1806年）にお
    いてもルーでとろみを付けるが血は用いられていない。このように、血で
    リエすることがシヴェの要件をなったのは比較的あたらしいことと想像さ
    れる。上記はリエーヴルのシヴェの例であり、次項の注のようにシュヴル
    イユのシヴェには血を用いないとあることから、エスコフィエが定義した
    可能性もある。
    
 
[^17]: 初版は「標準的なリエーヴルのシヴェとおなじように作るが、切り分
    けたシュヴルイユの肉を少なくとも6時間以上、煮汁にするワインと香辛
    料でマリネすること。シュヴルイユのシヴェは血でとろみを付けない。リ
    エーヴルのシヴェと同様に供する」とのみ。これが現行の「リエーヴルか
    繁殖うさぎの血でとろみを付ける」となったのは第二版から。


</div><!--Endrecette-->




### \hspace*{-1zw}シュヴルイユのコトレットとノワゼット {#cotelettes-et-noisettes}


<!--<div class="frsecbenv">Côtelettes et Noisettes</div>-->

シュヴルイユの[コトレット\*](#cotelette-gls)とノワゼット[^4]の掃除は仔
羊[^g40]の場合と同様におこなう。たいていは2切れで1人前と数える。他に調理法指
定がなければ、油で手早くソテーしたあと余分な油をきり、布でふきとる。バ
ターで焼き色を付けたハート型または丸型の薄いクルトンと交互にターバン状
に盛り付ける。

[^g40]: 本書で仔羊という場合は乳呑仔羊と\kenten{プレサレ}に限定されることに注意。

<!--[^14]: 背肉（主に骨付き）。-->

[^4]: 牛の背肉、仔牛モモ肉中心部の名称「ノワ」よりも小さい、主に羊、仔
羊、鹿のフィレ肉または背肉の中心部。この概説部分は初版のみ表現の異同あ
り。第二版からまったく同じ。初版ではノワゼットに1小節を別にあてており、
フィレミニョンの後に配置されている。収録されているレシピは現行の2つの
他、シュヴルイユのノワゼット・ワルキューレ。この3つのレシピは第三版ま
で。第四版つまり現行版でワルキューレは削除されている。







<div class="recette"> <!--beginRecette-->

#### シュヴルイユのコトレット・ボヴァル[^16] {#les-cotelettes-de-chevreuil-beauval}

\index{chevreuil@chevreuil!cotelette beauval@Côtelettes de --- Beauval}
\index{cotelette@côtelette!chevreuil beauval@--- de Chevreuil Beauval}
\index{beauval@Beauval!cotelette chevreuil@Côtelette de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとほうある@---のコトレット・ボヴァル}
\index{ことれつと@コトレット!しゆうるいゆほうある@シュヴルイユの---・ボヴァル}
\index{ほうある@ボヴァル!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---}



<div class="frsubenv">Côtelettes de Chevreuil Beauval</div>


小さな円盤型の[じゃがいも・ベルニ](#pommes-de-terre-berny)を小麦粉、溶
き卵、アーモンドスライスの順で衣を付けて油で揚げたものを冠状に並べ、そ
れぞれの上にソテーしたコトレットを置く。コトレットの上にグリル焼きした
マッシュルーム[^g41]をのせ、波模様の口金を取り付けた絞り袋で濃い[スビー
ズ](#sauce-soubise)をバラ模様に絞る。

ジュニパーベリー風味のクリームソースを別に供する。




[^16]: ボヴァルといえば現在はロワール地方サンテニャンにある巨大な動物
    園、サファリパーク（1980年開園）を指すが、「美しい谷」の意味をもつ
    この地名は複数あり、またボヴァルの姓を持つ歴史上の人物も少なくない。
    第四版のみ収録のため比較的あたらしいレシピと思われるが由来は不明。
 

[^g41]: [トゥルネ\*](#tourner-gls)してあること。そうしないとスビーズを絞った際に薔薇形にならない。



\atoaki{} <!--レシピ本体の後の空白を調整するためのコマンドなので、後にレシピが続くときは入れてください-->


#### シュヴルイユのコトレット・さくらんぼ添え {#cotelettes-de-chevreuil-aux-cerises}

\index{chevreuil@chevreuil!cotelette cerises@Côtelettes de --- aux Cerises}
\index{cotelette@côtelette!chevreuil cerises@--- de Chevreuil aux Cerises}
\index{cerise@cerise!cotelette chevreuil@Côtelette de chevreuil aux ---s}
\index{しゆうるいゆ@シュヴルイユ!ことれつとさくらんほ@---のコトレット・さくらんぼ添え}
\index{ことれつと@コトレット!しゆうるいゆさくらんほ@シュヴルイユの---・さくらんぼ添え}
\index{さくらんほ@さくらんぼ!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---添え}

<div class="frsubenv">Côtelettes de Chevreuil aux Cerises</div>

コトレットを血がしたたる程度に手早くソテーする。冠状に盛り、少し
固くなったパンデピス[^g42]をクルトン用にスライスし、軽くバターで色付け
して挟み込む。中央にさくらんぼのコンポートを盛り込む。
軽い[ソース・ヴネゾン](#sauce-venaison)を全体に覆いかける。



[^g42]: ジンジャーブレッドのこと。

\atoaki{}





#### シュヴルイユのコトレット・コンティ {#cotelettes-de-chevreuil-conti}

\index{chevreuil@chevreuil!cotelette conti@Côtelettes de --- Conti}
\index{cotelette@côtelette!chevreuil conti@--- de Chevreuil Conti}
\index{conti@Conti!cotelette chevreuil@Côtelette de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとこんてい@---のコトレット・コンティ}
\index{ことれつと@コトレット!しゆうるいゆこんてい@シュヴルイユの---・コンティ}
\index{こんてい@コンティ!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---}


<div class="frsubenv">Côtelettes de Chevreuil Conti[^15]</div>


煙が出るまで熱した油でソテーする。布で油を拭ってから、細長いハート形に薄く切っ
た[ロングエカルラット\*](#langue-ecarlate-gls)のスライスと交互になる
ように盛る。白ワイン少々で鍋を[デグラセ\*](#deglacer-gls)する。このデグラセした汁を軽くに仕上げた
[ソース・ポワヴラード](#sauce-poivrade-pour-gibier)に加え、コトレット
に塗る。

バターを少々加えた軽めの仕上がりのレンズ豆のピュレ[^6]を別皿で添える。


[^6]: ここではレンズ豆のピュレを用いることで料理名が決まっているといって
いい。詳しくは[ポタージュピュレ・コンティ」(#potage-puree-conti)および
[ガルニチュール・コンティ](#garniture-conti)訳注参照。また、併せて[ポ
タージュピュレ・コンデ](#potage-puree-conde)訳注も読まれたい。

[^15]: 初版、第二版は表現の異同が大きい。とりわけ初版では[ロングエカル
    ラット\*](#langue-ecarlate-gls)を「コトレットと同じ形状に切った[エ
    スカロップ\*](#escalope-gls)」にしているのが目をひく。そのほかで料理としてはほぼ変化がない。

\atoaki{}


#### シュヴルイユのコトレット・ディアーヌ[^7] {#cotelettes-de-chevreuil-diane}

\index{chevreuil@chevreuil!cotelette Diane@Côtelettes de --- Diane}
\index{cotelette@côtelette!chevreuil diane@--- de Chevreuil Diane}
\index{diane@Diane!cotelette chevreuil@Côtelette de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとていあぬ@---のコトレット・ディアーヌ}
\index{ことれつと@コトレット!しゆうるいゆていあぬ@シュヴルイユの---・ディアーヌ}
\index{ていあぬ@ディアーヌ!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---}


<div class="frsubenv">Côtelettes de Chevreuil Diane[^20]</div>

シュヴルイユのコトレットは手早くソテーして余計な油脂、水分をぬぐってか
らターバン状に盛る。[ジビエのファルス](#farce-gratin-c)を塗った三角形のクル
トンを肉と交互になるように挟み込む。クルトンに塗ったファルスは、バター
を薄く塗った天板に並べ、あらかじめオーブンに入れて[ポシェ\*](#pocher-gls)しておくこと。コ
トレットに[ソース・ディアーヌ](#sauce-diane)を塗り、深皿に盛った[^g43]マロン
のピュレを同時に供する。

[^7]: ローマ神話における月と豊穣、狩猟を司る女神。

[^20]: 料理内容はほぼおなじだが、初版のみ表現が異なる。「コトレットを
    ソテーし、三角形のクルトンと交互になるよう盛り付ける。クルトンには
    ジビエのファルスを塗っておく。ジビエのファルス・ムスリーヌは平らに
    7〜8 mm厚に塗り、オーブンの入口付近でポシェしておき、提供直前にカッ
    トする）。ソース・ディアーヌを塗り、栗のピュレを添えて供する
    (p.535)」

[^g43]: 原文 dresser en timbale （ドレセオンタンバル）。本書では多用さ
    れる表現だが、この場合は深皿に盛るの意。もとは野菜料理用の深皿に盛
    る、が原義。いわゆるタンバル仕立てとは異なるので注意が必要。

\atoaki{}


#### シュヴルイユのコトレット・ジュニエーヴル {#cotelettes-de-chevreuil-au-genievre}

\index{chevreuil@chevreuil!cotelette genievre@Côtelettes de --- au Genièvre}
\index{cotelette@côtelette!chevreuil genievre@--- de Chevreuil au Genièvr}
\index{genievre@genièvre!cotelette chevreuil@Côtelette de chevreuil au ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとていあぬ@---のコトレット・ジュニパーベリー風味}
\index{ことれつと@コトレット!しゆうるいゆていあぬ@シュヴルイユの---・ジュニパーベリー風味}
\index{しゆにはーへりー@ジュニパーベリー!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---風味}


<div class="frsubenv">Côtelettes de Chevreuil au Genièvre</div>

コトレットをソテーしてターバン状に盛り、それぞれに揚げ焼きしたハート型のクル
トンを挟み込む。\kenten{ジュニエーヴル}[^18]を1 dL弱[^19]注ぎフランベ
して、ソテー鍋を[デグラセ\*](#deglacer-gls)する。ジュ
ニパーベリーの乾燥実を1粒砕いて加え、クレームドゥーブル 1 dLを注いでに半量ま
で煮詰める。

仕上げにレモン果汁少量と大さじ何杯かの濃い[ソース・
ポワヴラード](#sauce-poivrade-pour-gibier)をこのソースに加える。コトレットに
塗る。

……別添で、軽く砂糖を加えただけのリンゴのマーマレードを温めて深皿に盛
り供する。


[^18]: 原文 eau de vie de genièvre ジュニパーベリー風味のブランデー。
    通常はジュニエーヴルとのみ称される。英語では Dutch gin オランダジ
    ン。北フランス、ベルギー、オランダ、北ドイツで製造されており、AOC
    を取得しているものも少なくない。
    
    
[^19]: 原文 un petit verre de （アンプチヴェールドゥ）ワイングラス1杯
    弱の。Un verre de は本書の場合 1 dL 相当であることに留意。

\atoaki{}


#### シュヴルイユのコトレット・アラミニュット[^22] {#cotelettes-de-chevreuil-a-la-minute}

\index{chevreuil@chevreuil!cotelette minute@Côtelettes de --- à la minute}
\index{cotelette@côtelette!chevreuil minute@--- de Chevreuil à la minute}
\index{minute@minute (à la)!cotelette chevreuil@Côtelette de chevreuil à la ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとみにゆつと@---のコトレット・アラミニュット}
\index{ことれつと@コトレット!しゆうるいゆみにゆつと@シュヴルイユの---・アラミニュット}
\index{あらみにゆつと@アラミニュット!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---}




<div class="frsubenv">Côtelettes de Chevreuil à la Minute</div>


ほんの少しの油で素早くソテーし、玉ねぎのごく細かい[アシェ\*](#hacher-gls)を少し振りかけ
る[^21]。ターバン状に盛り付ける。ソテー鍋にコニャックごく少量注いで[デ
グラセ\*](#deglacer-gls)する。[ソース・ポ
ワヴラード](#sauce-poivrade-pour-gibier) $\frac{1}{2}$ dL を加える。バター 50 gとレモン果汁少々を加えてソー
スを仕上げ、コトレットにかけ流す。

中央に[エマンセ\*](#emincer-gls)
したマッシュルームをバターでソテーして盛り込む。

\atoaki{}

[^21]: 現行版の原文 après les avoir saupoudrées d'un peu d'oignon
    haché très fin の après を前置詞ととるか副詞と見なすかで解釈が大き
    く変わる。常識的には、前置詞として理解し、後続の不定法が過去形のよ
    うに見えることから（実際は使役構文）「ごく細かい玉ねぎのアシェ少々
    を\kenten{振りかけてから}ソテーする」と解釈される可能性もあるが、
    ここでは初版の Sautées avec très peu d'huile, et saupoudrées d'un
    peu d'oignon haché fin 「ごく少量の油でソテーし、細かい玉ねぎのア
    シェ少量を振りかける」にしたがい、副詞として訳した (p.535)。
    
    
[^22]: 短時間で、の意。現代日本の調理現場では事前に仕込んでおかずに
    \kenten{その場で}調理することもアラミニュットと呼ぶ。
    
    
#### シュヴルイユのコトレット・ソースポワヴラード {#cotelettes-de-chevreuil-sauce-poivrade}

\index{chevreuil@chevreuil!cotelette poivrade@Côtelettes de --- sauce Poivrade}
\index{cotelette@côtelette!chevreuil poivrade@--- de Chevreuil sauce Poivrade}
\index{poivrade@poivrade!cotelette chevreuil@Côtelette de chevreuil sauce ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとほわうらと@---のコトレット・ソースポワヴラード}
\index{ことれつと@コトレット!しゆうるいゆほわうらと@シュヴルイユの---・ソースポワヴラード}
\index{ほわうらと@ポワヴラード!ことれつとしゆるいゆ@シュヴルイユのコトレット・ソース---}


<div class="frsubenv">Côtelettes de Chevreuil sauce Poivrade[^23]</div>


ソテーし、余計な油脂と水分をぬぐってから[ソース・ポワヴラー
ド](#sauce-poivrade-pour-gibier)を塗り、揚げ焼きした薄いクルトンと交互
にターバン状に盛る。栗のピュレまたは根セロリのピュレを別添で供する。

###### 【原注】 {#nota-cotelettes-de-chevreuil-sauce-poivrade}

大型ジビエの項で栗のピュレを添えると指示しているいくつかの箇所
は、さつまいものピュレに代えてもいい。

[^23]: 初版はクルトンへの言及なし。「好みのガルニチュールを添える
    (p.535)」となっている。それ以外、本文は第二版以降異同なし。原注は
    第四版のみ。

\atoaki{}


#### シュヴルイユのコトレット・トリュフ添え {#cotelettes-de-chevreuil-aux-truffes}


\index{chevreuil@chevreuil!cotelette truffes@Côtelettes de --- aux Truffes}
\index{cotelette@côtelette!chevreuil truffes@--- de Chevreuil aux Truffes}
\index{truffe@truffe!cotelette chevreuil@Côtelette de chevreuil aux ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつととりゆふふうみ@---のコトレット・トリュフ風味}
\index{ことれつと@コトレット!しゆうるいゆとりゆふふうみ@シュヴルイユの---・トリュフ風味}
\index{とりゆふ@トリュフ!ことれつとしゆるいゆ@シュヴルイユのコトレット・トリュフ風味}




<div class="frsubenv">Côtelettes de Chevreuil aux truffes (*1-4*)</div>

澄ましバターでソテーし、ターバン状に盛る。トリュフエッセンスでソテー鍋
を[デグラセ\*](#deglacer-gls)して、必要量の[ソース・マデー
ル](#sauce-madere)と、コトレット1枚につきのトリュフのスライス 4 枚を加
える。ソースを塗り、上にトリュフのスライスを並べる。



\atoaki{}


#### シュヴルイユのコトレット・ヴィルヌーヴ[^24] {#cotelettes-de-chevreuil-villeneuve}

\index{chevreuil@chevreuil!cotelette villeneuvetoday@Côtelettes de --- Villeneuve}
\index{cotelette@côtelette!chevreuil villeneuve@--- de Chevreuil Villeneuve}
\index{villeneuve@Villeneuve!cotelette chevreuil@Côtelette de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!ことれつとういるぬう@---のコトレット・ヴィルヌーヴ}
\index{ことれつと@コトレット!しゆうるいゆういるぬう@シュヴルイユの---・ヴィルヌーヴ}
\index{ういるぬう@ヴィルヌーヴ!ことれつとしゆうるいゆ@シュヴルイユのコトレット・---}


<div class="frsubenv">Côtelettes de Chevreuil Villeneuve</div>


シュヴルイユのコトレットはバターで焼き色が付かないよう手早く表面を焼き固める[^25]。
軽く重しをのせて冷ます。ジビエの冷製[サルピコン](#salpicons-divers)をなめらかなドーム状に盛
り、三角形の網脂でそれぞれのコトレットをつつみ、天板に並べる。溶かしバ
ターを[アロゼ\*](#arroser-gls)し、オーヴンで7〜8分間火入れする。ターバ
ン状に盛り付け、ごく細いトリュフの[ジュリエンヌ\*](#julienne-gls)を加
えたジビエの[クリ](#coulis-divers)[^g45]を別添で供する。

[^24]: エスコフィエの生地、ヴィルヌーヴ・ルベにちなんだものか。他の可
    能性としては、あまりにも一般的でこんにちでは人名（名字）のひとつと
    して、あるいは地名（の一部）として認識されるのみだろう。プロヴァン
    ス地方およびラングドック地方に別々の系統のヴィルヌーヴ家という貴族
    の家系があり、後者については十字軍に三人兄弟揃って参加したアルノー・
    ドゥ・ヴィルヌーヴが比較的有名か。また、19世紀にエミール・ヴィルヌー
    ヴという急進的左派の政治家がいたが、料理とは関係ないと思われる。

[^25]: 初版は「強火で[レディール\*](#raidir-gls)する (p.536)」とある。
    表面を焼き固めるだけが目的だから、手早く作業する必要がある。
    レディールは焼き色を付けてはいけない。ともすれば弱火にしたくな
    るかも知れないが、本来強火でおこなう作業と思っていい。

[^g45]: クリとは現代的な理解ではピュレよりも濃度の低いものを指すが、18
    世紀においてはソースとほぼ同義であったことに留意したい。ここではリエしていないソースの意。

\atoaki{}

<!--20190912江畑校正確認スミ-->

#### シュヴルイユのノワゼット・ロマノフ[^8] {#noisettes-de-chevreuil-romanoff}

\index{chevreuil@chevreuil!noisettes romanoff@Noisettes de --- Romanoff}
\index{noisette@noisette!chevreuil romanoff@--- de Chevreuil Romanoff}
\index{romanoff@Romanoff!noisette chevreuil@Noisettes de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!のわせつとろまのふ@---のノワゼット・ロマノフ}
\index{のわせつと@ノワゼット!しゆうるいゆろまのふ@シュヴルイユの---・ロマノフ}
\index{ロマノフ@ロマノフ!のわせつとしゆうるいゆ@シュヴルイユのノワゼット・---}



<div class="frsubenv">Noisettes de Chevreuil Romanoff[^30]</div>


ノワゼットを強火でソテーする。[コンコンブル\*](#concombre-gls)を[トロ
ンソン\*](#troncon-gls)に切って凹みを作りブレゼしておく[^g44]。提供直前にこれを冠
状に盛りマッシュルームの濃いピュレを凹みに詰める。その上にノワゼットを
置く。中央に[セープ茸のクリーム煮](#cepes-a-la-creme)を盛り込む。

[ソース・ポワヴラード](#sauce-poivrade-pour-gibier)を別添で供する。

[^30]: 上述のように初版のみノワゼットが別の小節に分けられているので原書対照の際には注意。

[^8]: 1613~1917年までロシアを統治したリトアニア出身ロマノフ家の事。

[^g44]: 野菜のブレゼについては注意が必要。[野菜のブレゼ](#braisage-des-legumes)参照。

\atoaki{}


#### シュヴルイユのノワゼット・ヴァレンシア[^26] {#noisettes-de-chevreuil-valencia}

\index{chevreuil@chevreuil!noisettes valencia@Noisettes de --- Valencia}
\index{noisette@noisette!chevreuil valencia@--- de Chevreuil Valencia}
\index{valencia@Valencia!noisette chevreuil@Noisettes de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!のわせつとうあれんしあ@---のノワゼット・ヴァレンシア}
\index{のわせつと@ノワゼット!しゆうるいゆろまのふ@シュヴルイユの---・ロマノフ}
\index{うあれんしあ@ヴァレンシア!のわせつとしゆうるいゆ@シュヴルイユのノワゼット・---}


<div class="frsubenv">Noisettes de Chevreuil Valencia (*1-4*)</div>


ノワゼットをソテーする。バターで揚げ焼きした丸いブリオッシュのクルトン
の上に1つづつのせて冠状に盛る。軽く[ソース・ビガラード](#sauce-bigarade)を塗り、櫛切りにし
て完全に皮をむいたオレンジで囲って飾る。

[ソース・ビガラード](#sauce-bigarade)を別添えする。


[^26]: スペインのバレンシア地方のこと。

\atoaki{}

</div><!--endRecette-->



### \hspace*{-1zw}キュイソ {#cuissot}

\index{cuissot@cuissot!chevereuil@--- (de chevreuil)}
\index{chevreuil@chevreuil!cuissot@cuissot}
\index{しゆうるいゆ@シュヴルイユ!きゆいそ@キュイソ}
\index{きゆいそ@キュイソ!しゆうるいゆ@シュヴルイユの---}　

キュイソ[^39]は丁寧に筋を取り除き、細かく[ピケ\*](#piquer-gls)すること。
数時間マリネするか、マリネせずにローストするかは場合による。

ガルニチュールととソースはどれもセル用がよく合うので、[シュヴルイユのセルの項
目](#selle-ou-cimier)を参照[^31]。

[^31]: 初版のみ表現に若干の異同はあるが内容はおなじ。

[^39]: 大型ジビエのもも肉のことは cuissot （キュイソ）と呼ぶ。



### \hspace*{-1zw}フィレミニョン {#chevreuil-filets-mignons}

\index{filet mignon@filet mignon!chevreuil@--- de chevreuil}
\index{chevreuil@chevreuil!filets mignons@filets mignons}
\index{しゆうるいゆ@シュヴルイユ!ふいれみによん@フィレミニョン}
\index{ふいれみによん@フィレミミョン!しゆうるいゆ@シュヴルイユの---}



<div class="recette"><!--beginRecette-->


#### シュヴルイユのフィレミニョン・ジュニエーヴル {#filets-mignons-de-chevreuil-au-genievre}

\index{chevreuil@chevreuil!filets mignons genievre@Filets mignons de --- au Genièvre}
\index{filet mignon@filet mignon!chevreuil genievre@--- de Chevreuil au Genièvre}
\index{genievre@genièvre!filets mignons chevreuil@Filets minons de chevreuil --- au Genièvre}
\index{しゆうるいゆ@シュヴルイユ!ふいれみによんしゆにはーへりー@---のフィレミニョン・ジュニパーベリー風味}
\index{ふいれみによん@フィレミニョン!しゆうるいゆしゆにはーへりー@シュヴルイユの---・ジュニパーベリー風味}
\index{しゆにはーへりー@ジュニパーベリー!しゆうるいゆのふいれみによん@シュヴルイユのフィレミニョン・---風味}


<div class="frsubenv">Filets mignons de Chevreuil au Genièvre</div>


余分な筋、脂を取り除き整形して、[ピケ\*](#piquer-gls)する。ローストし、
長い大皿に盛る。

ソースとガルニチュールは[コトレット・ジュニエーヴ
ル](#cotelettes-de-chevreuil-au-genievre)とまったくおなじ。



\atoaki{}


#### シュヴルイユのフィレのタンバル[^g46]・ナポリ風 {#timbale-de-filets-de-chevreuil-a-la-napolitaine}

\index{chevreuil@chevreuil!rimbale filets napolitaine@Timbale de Filets de --- à la Napolitaine}
\index{filet mignon@filet mignon!timbale chevreuil napolitaine@Tambale de --- de Chevreuil à la Napolitaine}
\index{napolitain@napolitain(e)!Timbale filets chevreuil@Timbale de Filets de chevreuil à la ---e}
\index{しゆうるいゆ@シュヴルイユ!ふいれのたんはるなほりふう@---のフィレのタンバル・ナポリ風}
\index{ふいれみによん@フィレミニョン!しゆうるいゆたんはるなほりふう@シュヴルイユの---のタンバル・ナポリ風}
\index{なほりふう@ナポリ風!しゆうるいゆのふいれのたんはる@シュヴルイユのフィレのタンバル・---風}

<div class="frsubenv">Timbale de Filets de chevreuil à la Napolitaine</div>



フィレを[エスカロップ\*](#escalope-gls)に切り分ける。細かく[ピケ
\*](#piquer-gls)し、加熱した白ワインの[マリナー
ド](#marinade-crue-ou-cuite-pour-grosse-venaison)で何時間かマリネする。

少量の[フォンドヴォー](#fonds-ou-jus-de-veau-brun)とマリナードで[ブレ
ゼ](#braisee)し、最後に高温のオーブンでこのフォンを煮詰め[グラセ\*](#glacer-gls)する<!--[^28]-->。

マカロニをさっと[ブロンシール\*](#blanchir-gls)して、[白いコンソメ](#consomme-blanc-simple)でしっ
かり火入れする。よく水気を切り、[ラペ\*](#raper-gls)したパルメザンチーズ、フレッシュな
バター、トマトのピュレ、[ジビエのグラス](#glaces-diverses)、フィレミニョン<!--の
蒸し煮の-->をブレゼした際の煮汁を加えて[リエ\*](#lier-gls)する。空焼きした高さ
のないタンバルにマカロニ[^g47]を詰めて、その上に高温のオーブンに入れて
グラセしたフィレミニョンを冠状に盛り込む。

[^g46]: いわゆるタンバル仕立て。

<!--[^28]: glacer（グラセ）。-->

[^g47]: 本書において[マカロニ](#macaroni)は「円筒形の長さのあるパスタ」
    とのみ定義されており、中空になっているものに限定されず、スパゲッティ
    なども含まれることに注意。

\atoaki{}


#### シュヴルイユのフィレミニヨン・ソース・ヴネゾン {#filets-mignons-de-chevreuil-sauce-venaison}

\index{chevreuil@chevreuil!Filets mignons sauce Venaison@Filets mignons de --- sauce Venaison}
\index{filet mignon@filet mignon!timbale chevreuil napolitaine@Tambale de --- de Chevreuil à la Napolitaine}
\index{sauce venaison@sauce Venaison!filets mignons de chevreuil@Filets mignons de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!ふいれみによんそーすうねそん@---のフィレミニョン・ソースヴネゾン}
\index{ふいれみによん@フィレミニョン!しゆうるいゆそーすうねそん@シュヴルイユの---・ソース・ヴネゾン}
\index{そーすうねそん@ソースヴネゾン!しゆうるいゆのふいれみによん@シュヴルイユのフィレミニヨン・---}


<div class="frsubenv">Filets mignons de chevreuil sauce Venaison</div>

[ピケ\*](#piquer-gls)して、ソテーする。冠状に盛り付け、[ソース・ヴネゾ
ン](#sauce-venaison)を塗る。

野菜料理用の深皿に盛った栗の軽いピュレ、もしくはセロリのピュレを別添で
供する[^32]。

[^32]: 初版の内容は「ピケし、ソテーしてソース・ヴネゾンをかける。ガル
    ニチュールは何でもいいが、付けなくてもいい(p.536)」。





</div><!--Endrecette-->




### \hspace*{-1zw}セル、シミエ {#selle-ou-cimier}



場合によるが、セル[^29]は単にカレからキュイソまでの間と理解され、それ以外に
胸側をほとんど取り去った1対のカレも付け加えられる。いずれにしても
切り込む先は腰部の尾の付け根からの対角線に沿う。
通常、余分な脂と筋を丁寧に取り除いてから[ピケ\*](#piquer-gls)する。



[^29]: 羊などの場合 selle （セル）は通常「鞍下肉」と訳すが、
場合によっては胸部の肋骨をほとんど取り去った1対（2本分）の carré（カレ）も指す。
「尾の付け根からの対角線に沿う」とはつまり、背骨に沿って切る事を意味している。



<div class="recette">

#### シュヴルイユのセル・ブリアン {#selle-de-chevreuil-briand}

\index{chevreuil@chevreuil!Selle Briand@Selle de --- Briand}
\index{selle@selle!chevreuil briand@--- de chevreuil Biriand}
\index{briand@Briand!selle chevreuil@Selle de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!せるふりあん@---のセル・ブリアン}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!シュヴルイユの---・ブリアン}
\index{ふりあん@ブリアン!しゆうるいゆのせる@シュヴルイユのセル・---}


<div class="frsubenv">Selle de Chevreuil Briand[^33])</div>

セルは12時間マリネする。火を通す時に水気をぬぐい、天板の底に散らした[マ
リナード](#marinade-crue-ou-cuite-pour-grosse-venaison)の香味野菜の上に載せて、強火のオーヴンでローストする。盛り付け
をし、赤ワインに少量の砂糖、香辛料としてシナモンとレモンの[ゼスト
\*](#zeste-gls)を加えて調理した洋梨を各々の端に並べて盛る。

天板に2 dLの[ジビエのフォン](#fonds-de-gibier)を注ぎ入れ、10分間火にかける。漉してから[デ
グレセ\*](#degraisser-gls)しアロールートで[リエ\*](#lier-gls)する。この、リエしたフォンと、赤すぐりのジュレと同時に別添にて供する。


[^33]: 初版〜第三版は「シュヴルイユのセル・バーデン=バーデン」の料理名。
    そしてその直前に「シュヴルイユのセル・ドイツ風」がある。何らかの手
    違いでドイツ風を名称変更するつもりが、次のバーデン=バーデンの名称
    変更になってしまったとも考えられる。バーデン=バーデンはドイツ西部、
    ストラスブールにやや近い都市のことだから、どちらを改名してもエスコ
    フィエのドイツ嫌いは満足されることになる。つまりはソース・アルマン
    ドをソース・パリジエンヌと名称だけ変えたのと根本的におなじ構図であ
    り、ブリアンという名称の由来など気にする必要もないだろう。

\atoaki{}


#### シュヴルイユのセル・スリーズ[^g48] {#selle-de-chevreuil-aux-cerises}

\index{chevreuil@chevreuil!Selle cerises@Selle de --- aux Cerises}
\index{selle@selle!chevreuil cerises@--- de chevreuil aux Cerises}
\index{cerise@cerise!selle chevreuil@Selle de chevreuil aux ---s}
\index{しゆうるいゆ@シュヴルイユ!せるさくらんほそえ@---のセル・さくらんぼ添え}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!さくらんほそえ@シュヴルイユの---・さくらんぼ添え}
\index{さくらんほ@さくらんぼ!しゆうるいゆのせる@シュヴルイユのセル・---添え}

<div class="frsubenv">Selle de Chevreuil aux Cerises</div>


セルを12時間、ヴェルジュ[^34]の[マリナード](#marinade-crue-ou-cuite-pour-grosse-venaison)に漬けておく。焼き串を刺して
マリナードを[アロゼ\*](#arroser-gls)しながらあぶり焼きローストして、焼
き加減をロゼにする。

盛り付けて、[ソース・ポワヴラード](#sauce-poivrade-pour-gibier)、また
は[ソース・ヴネゾン](#sauce-venaison)か[ソース・スリー
ズ](#sauce-aux-cerises)を別添で供する。（[ルーアン仔鴨・スリー
ズ](#caneton-rouennais-aux-cerises)参照）


[^34]: verjus 未熟ぶどう果汁およびそれを原料とした酸味料。甘みもあって
    それだけでも美味。中世にはヴィネガー（ワインをもとにしたもの）より
    もむしろ好まれた。

[^g48]: スリーズ cerise はさくらんぼのことだが、ソース・スリーズが名称
    のみで実際にさくらんぼを使わないこと、[シュヴルイユのコトレット・
    さくらんぼ添え](#cotelettes-de-chevreuil-aux-cerises)とは異なり、こ
    の料理そのものにもさくらんぼが使われていないことから、カナ書きした。

\atoaki{}



#### シュヴルイユのセル・シェルヴィル[^10] {#selle-de-chevreuil-cherville}

\index{chevreuil@chevreuil!Selle cherville@Selle de --- Cherville}
\index{selle@selle!chevreuil cherville@--- de chevreuil Cherville}
\index{cherville@Cherville!selle chevreuil@Selle de chevreuil aux ---s}
\index{しゆうるいゆ@シュヴルイユ!せるしえるういる@---のセル・シェルヴィル}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!しえるういる@シュヴルイユの---・シェルヴィル}
\index{しゆうるいゆ@シュヴルイユ!しゆうるいゆのせる@シュヴルイユのセル・---}

<div class="frsubenv">Selle de Chevreuil Cherville</div>


マリネせずセルに串を刺してあぶり焼きローストする。レネット種のリンゴ
[^9]をくり抜いて器にし、中にりんごの果肉の[エマンセ\*](#emincer-gls)を
入れて、オーヴンで火入れしたものでセルを囲んで盛り付ける。

……レフォール入り[ソース・グロゼイユ](#sauce-groseilles)を別に供する。

[^9]: レネット種は香りの強い<!--デザート用-->りんご。<!--用語集参照。-->

[^10]: シャンパーニュ地方の小都市の名だが、cher (シェール、女性形
    chère、大切な、愛しい) + ville (街)の意味が読みとれる。第一次大戦
    後の愛国心で高揚したフランスの雰囲気が伝わるような料理名だろう。



\atoaki{}



#### シュヴルイユのセル・クレーム {#selle-de-chevreuil-a-la-creme}

\index{chevreuil@chevreuil!selle creme@Selle de --- à la crème}
\index{selle@selle!chevreuil creme@--- de chevreuil à la crème}
\index{creme@crème (à la)!selle chevreuil@Selle de chevreuil à la ---}
\index{しゆうるいゆ@シュヴルイユ!せるくれむ@---のセル・クレーム}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!くれむ@---・クレーム}
\index{しゆうるいゆ@シュヴルイユ!せるくれむ@---セル・クレーム}


<div class="frsubenv">Selle de Chevreuil à la Crème</div>


非加熱で用意した[マリナー
ド](#marinade-crue-ou-cuite-pour-grosse-venaison)にセルを 2 〜 3 日間つけておく。

小さな天板の底にマリナードの香味野菜を敷いてローストする。セルに火
が通ったらすぐに取りだし、少量のマリナードで天板を[デグラセ
\*](#deglacer-gls)し、ほぼ完全に煮詰める。[デグレセ
\*](#degraisser-gls)する。生クリーム 3 dLとジュニパーベリー1粒を砕いて
加える。さらに $\frac{2}{3}$ 量まで煮詰め、溶かした[グラスドヴィアン
ド](#glace-de-viande)少々で仕上げ、こし布でこす。

ソースとセルを同時に供する。


\atoaki{}



#### シュヴルイユのセル・クレオール風[^35] {#selle-de-chevreuil-a-la-creole}

\index{chevreuil@chevreuil!selle creole@Selle de --- à la Créole}
\index{selle@selle!chevreuil creole@--- de chevreuil à la Créole}
\index{creole@créole (à la)!selle chevreuil@Selle de chevreuil à la ---}
\index{しゆうるいゆ@シュヴルイユ!せるくれおる@---・クレオール風}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!くれおるふう@シュヴルイユの---・クレオール風}
\index{くれおるふう@クレオール風!しゆうるいゆのせる@シュヴルイユのセル・---}



<div class="frsubenv">Selle de Chevreuil à la Créole</div>


何時間かマリネするだけで串刺しにして、マリナードを[アロゼ\*](#arroser-gls)しながらローストする。

長い大皿に盛り、バターでソテしたバナナで囲む。

[ソース・ロベール・エスコフィエ](#sauce-robert-escoffier)[^36]に
$\frac{1}{3}$ 量の[ソース・ポワヴラード](#sauce-poivrade-pour-gibier)
を足して軽くバターを加えて仕上げ、セルと同時に供する。


[^35]: créole （クレオール）旧植民地におけるヨーロッパ人の子孫、が原義
    だが、料理の場合は熱帯地方の、とりわけハイチなど島々の産物を使って
    いたり、その土地の料理に影響をうけたものにこの名称が付けられること
    が多い。このレシピでもバナナのソテーをガルニチュールにしていること
    が名称の根拠となる。

[^36]: 既製品であることに注意。当該項目参照。



\atoaki{}



#### シュヴルイユのセル・ジュニエーヴル {#selle-de-chevreuil-au-genievre}

\index{chevreuil@chevreuil!selle genievre@Selle de --- au Genièvre}
\index{selle@selle!chevreuil genievre@--- de chevreuil au Genièvre}
\index{genievre@genièvre!selle chevreuil@Selle de chevreuil au ---}
\index{しゆうるいゆ@シュヴルイユ!せるしゆにえふる@---のセル・ジュニエーヴル}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!しゆにえふる@シュヴルイユの---・ジュニエーヴル}
\index{しゆにえふる@ジュニエーヴル!しゆうるいゆのせる@シュヴルイユのセル・---}




<div class="frsubenv">Selle de Chevreuil au Genièvre</div>

セルをピケしてローストする。ソースは[コトレット・ジュニエーヴ
ル](#cotelettes-de-chevreuil-au-genievre)とおなじ作りかたで、おなじく
りんごのマーマレードを別添で供する。



\atoaki{}


#### シュヴルイユのセル・グランヴヌール（スコットランド風[^37]） {#selle-de-chevreuil-grand-veneur}

\index{chevreuil@chevreuil!selle grand veneur@Selle de --- Grand Veneur}
\index{selle@selle!chevreuil grand veneur@--- de chevreuil Grand Veneur}
\index{grand veneur@grand veneur!selle chevreuil@Selle de chevreuil ---}
\index{しゆうるいゆ@シュヴルイユ!せるくらんうぬる@---のセル・グランヴヌール}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!くらんうぬる@シュヴルイユの---・グランヴヌール}
\index{くらんうぬる@グランヴヌール!しゆうるいゆのせる@シュヴルイユのセル・---}



<div class="frsubenv">Selle de Chevreuil Grand Veneur</div>

セルを白ワインと香味野菜のマリナードで2時間マリネする。

よく拭いてから豚背脂のシートで包みローストする。

大皿に[ソース・ポワヴラー
ド](#sauce-poivrade-pour-gibier)を塗り、その上に盛り付ける。型に入れて球形に作ったじゃがいものクロ
ケットを周囲に盛り込む。

別添で[ソース・ヴネゾン](#sauce-venaison)、栗のピュレ、
野菜皿に盛った茹でたてのアリコヴェール[^38]をそれぞれ供する。

[^37]: 貴族や王家に仕える狩猟長のことをグランヴヌールと呼ぶが、ここで
    はソース・グランヴヌールを用いていないことに注意。
    

[^38]: いわゆる「さやいんげん」のことだが、日本の青果で一般的なそれと
    はサイズ、見た目、品種、調理特性、味、風味が大きく異なるため、カナ
    書きしてある。

\atoaki{}


#### シュヴルイユのセル、さまざまなソースで {#selle-de-chevreuil-avec-diverses-sauces}

\index{chevreuil@chevreuil!selle diverses sances@Selle de --- avec diverses sauces}
\index{selle@selle!chevreuil diverses sauces@--- de chevreuil avec diverses sances}
<!--\index{grand veneur@grand veneur!selle chevreuil@Selle de chevreuil ---}-->
\index{しゆうるいゆ@シュヴルイユ!せるさまさまなそーすて@---のセル・さまざまなソースで}
\index{せるしゆうるいゆ@セル（シュヴルイユ）!さまさまなそーすて@シュヴルイユの---・さまざまなソースで}
<!--\index{くらんうぬる@グランヴヌール!しゆうるいゆのせる@シュヴルイユのセル・---}-->



<div class="frsubenv">Selle de Chevreuil avec diverses Sauces</div>

<!--ピケしたセルをマリネ、もしくはマリネしないでローストして、次のソース各種と合わせられる。-->

セルは[ピケ\*](#piquer-gls)し、マリネしてもしなくてもいいが、ロースト
して以下のソースを合わせる。

[ディアンヌ](#sauce-diane)---[グランヴヌール](#sauce-grand-veneur)---[モス
コヴィット](#sauce-moscovite)---[ポワヴラー
ド](#sauce-poivrade-pour-gibier)---[ヴネゾン](#sauce-venaison)など。


\atoaki{}

\index{chevreuil@chevreuil|)}
\index{しゆうるいゆ@シュヴルイユ|)}


</div><!--Endrecette-->


<!--20190914江畑校正スミ-->

