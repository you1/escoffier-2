---
author: '五 島　学　編（訳・注釈）'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
title: 'エスコフィエ『フランス料理の手引き』全注解'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->



<!--このあとから本文を書いてください。-->



## ソングリエとマルカサン[^1] {#sanglier-et-marcassin}

\index{sanglier}
\index{marcassin}
\index{そんくりえ@ソングリエ}
\index{まるかさん@マルカサン}
\index{いのしし@猪|see マルカサン}
<!--<div class="frsecbenv">SANGLIER ET MARCASSIN</div>-->

マルカサンと呼ばれる月齢を過ぎソングリエになればキュイソ[^2]以外はほとんど
料理には使われず、その場合でも強い[マリナード](#marinade-crue-ou-cuite-pour-grosse-venaison)になじませなければならない。
若いサングリエ、すなわちマルカサンは逆にとても珍重される。
ノワゼット、フィレ又はカルティエのマルカサンはシュヴルイユの様に下拵え
出来る。カレとセルの塊肉ローストも同様。「[シュヴルイユのコトレット](#cotelettes-et-noisettes)」
に示されたやり方は同じくマルカサンのセルに当てはまる。



[^1]: 一般に6ヶ月齢までの仔猪を marcassin（マルカサン）、それ以降の猪
    （成獣）を sanglier （ソングリエ）と呼ぶ。日本でも古くから
    \kenten{山鯨}と呼ばれ食材としてなじみのある大型ジビエだが、日本で
    は初冬の脂ののった成獣が珍重されるのにたいして、フランスでは本文に
    あるように若い猪のほうが食材として高く評価されている。なお、牛と似
    てマルカサンは白身肉だがソングリエは基本的に赤身肉であり、日本では
    雌で 70〜80 kg くらいの成獣がよいとされている。当然ながらおなじ猪
    とはいっても食材としての特性が大きく異なるため、猪と訳さずにフラン
    ス語をカナ書きしてある。
    
[^2]: 大型ジビエの\kenten{もも肉}を cuissot （キュイソ）と呼ぶ。

<!--\atoaki{}-->
<div class="recette">

#### マルカサンのコトレット・フランドル風[^flamande] {#cotelettes-de-marcassin-a-la-flamande}

\index{marcassin@marcassin!cotelettes flamande@Côtelettes de --- à la Flamande}
\index{cotelette marcassin@côtelette (marcassin)!flamande@--- de Marcassin à la Flamande}
\index{flamand@flamand(e)!cotelettes marcassin@Côtelettes de Marcassin à la ---e}
\index{まるかさん@マルカサン!ことれつとふらんとるふう@---のコトレット・フランドル風}
\index{ことれつとまるかさん@コトレツト（マルカサン）!まるかさんふらんとるふう@マルカサンの---・フランドル風}
\index{ふらんとるふう@フランドル風!まるかさんのことれつと@マルカサンのコトレット・---}

<div class="frsubenv">Côtelettes de marcassin à la Flamande (1-4)</div>

[豚のコット・フラマンド風](#cote-de-porc-a-la-flamande)と同様に調理する。

[^flamande]: フランドル地方　北海に面する地方で、現在のベルギー西部とフランス北部にまたがる。


\atoaki{}


#### マルカサンのコトレット・サンテュベール {#cotelettes-de-marcassin-saint-hubert}

\index{marcassin@marcassin!cotelettes saint hubert@Côtelettes de --- Saint-Hubert}
\index{cotelette marcassin@côtelette (marcassin)!saint hubert@--- de Marcassin Saint-Hubert}
\index{saint hubert@Saint-Hubert!cotelettes marcassin@Côtelettes de Marcassin ---}
\index{まるかさん@マルカサン!ことれつとさんちゆへーる@---のコトレット・サンチュベール}
\index{ことれつとまるかさん@コトレツト（マルカサン）!まるかさんさんちゆへーる@マルカサンの---・サンチュベール}
\index{さんちゆへーる@サンチュベール!まるかさんのことれつと@マルカサンのコトレット・---}

<div class="frsubenv">Côtelettes de marcassin Saint-Hubert (1-4)</div>

コトレットを片面だけソテし、重しをかけて冷ます。

マルカサンの脂身 800 g、バターで色づかないように炒めたムスロン 200 g、
細かく砕いたジュニパーベリー 2 粒できめ細かなファルスを作り、通常の調
味をほどこして、焼き面に盛る。アミ脂でファルスを盛ったコトレットを包み、
天板に並べ、溶かしバターで[アロゼ\*](#arroser-gls)し、パン粉をふりかけオーヴンで火入れする。

別添で (1) [ソース・ヴネゾン](#sauce-venaison)、(2) 砂糖を入れないリン
ゴのマーマレード、を供する。



\atoaki{}



#### マルカサンのコトレット・ローマ風 {#cotelettes-de-marcassin-a-la-romaine}

\index{marcassin@marcassin!cotelettes romaine@Côtelettes de --- à la Romaine}
\index{cotelette marcassin@côtelette (marcassin)!romaine@--- de Marcassin à la Romaine}
\index{romain@romain(e)!cotelettes marcassin@Côtelettes de Marcassin à la ---e}
\index{まるかさん@マルカサン!ことれつとろまふう@---のコトレット・ローマ風}
\index{ことれつとまるかさん@コトレツト（マルカサン）!まるかさんろまふう@マルカサンの---・ローマ風}
\index{ろまふう@ローマ風!まるかさんのことれつと@マルカサンのコトレット・---}

<div class="frsubenv">Côtelettes de marcassin à la Romaine (1-4)</div>

コトレットをソテし、ターバン状に盛る。[ローマ風ソース](#sauce-romaine)で覆う。

マロン[^9]のピュレ、セロリのピュレもしくはレンズ豆のピュレを別に供する[^3]。

[^3]: 初版ではガルニチュールは任意でよいと記されている。
 
[^9]: 栗の一種だが、\kenten{いが}にひとつの実しか入っていない。当然、
    味、風味、調理特性は和栗と異なる。また調理した際の色合いも違うので
    日本の栗では代用がきかない。
    
\atoaki{}


#### マルカサンのコトレット・サンマルコ {#cotelettes-de-marcassin-saint-marc}

\index{marcassin@marcassin!cotelettes saint-marc@Côtelettes de --- Saint-Marc}
\index{cotelette marcassin@côtelette (marcassin)!saint-marc@--- de Marcassin Saint-Marc}
\index{saint-marc@Saint-Marc!cotelettes marcassin@Côtelettes de Marcassin ---}
\index{まるかさん@マルカサン!ことれつとさんまるこ@---のコトレット・サンマルコ}
\index{ことれつとまるかさん@コトレツト（マルカサン）!まるかさんさんまるこ@マルカサンの---・サンマルコ}
\index{さんまるこ@サンマルコ!まるかさんのことれつと@マルカサンのコトレット・---}


<div class="frsubenv">Côtelettes de marcassin Saint-Marc (1-4)</div>

[ロングエカルラット\*](#langue-ecarlate-gls)をコトレットに[ピケ\*](#piquer-gls)して[^8][ブレゼ](#les-braises)する。

冠状に盛り、中央に、球形の型に入れて作ったマロンの小さなクロケットをピラミッド状に盛り込む。

ブレゼの煮汁を[デグレセ\*](#degraisser-gls)して煮詰めたものと、クランベリーのマーマレードを
ソーシエールに入れて別々に供する（[クランベリーソース](#cranberries-sauce)）。


[^8]: [ピケ\*](#piquer-gls)については第三版から。

\atoaki{}


#### マルカサンのキュイソ・トゥール風{#cuissot-de-marcassin-a-la-mode-tours}

\index{marcassin@marcassin!cuissot tours@Cuissot de --- à la mode de Tours}
\index{cuissot marcassin@cuissot (marcassin)!tours@--- de Marcassin à la mode de Tours}
\index{saint-marc@Saint-Marc!cotelettes marcassin@Côtelettes de Marcassin ---}
\index{まるかさん@マルカサン!きゆいそとうーる@---のキュイソ・トゥール風}
\index{きゆいそまるかさん@キュイソ（マルカサン）!まるかさんとうーる@マルカサンの---・トゥール風}
\index{とうーるふう@トゥール風!まるかさんのきゆいそ@マルカサンのキュイソ・---}

<div class="frsubenv">Cuissot de marcassin à la mode Tours (4)</div>

加熱[マリナード](#marinade-crue-ou-cuite-pour-grosse-venaison)（ヴネゾン用マリナード参照）に5〜6日間キュイソを漬け込ん
でおく。このマリナードには普通の白ワインではなくトゥレーヌ酸のものを使
うこと。

水分をしっかり拭きとって、塩漬け豚バラ肉 250 g をさいの目に切り、一緒
にブレゼ鍋に入れる。

肉が焼き固まったら、塩で調味し、トマト 6 個分の[アシェ\*](#hacher-gls)とマリナードを加
える。できたらオーブンでおだやかに火入れすること。

提供直前に、キュイソを盛り付ける。煮汁は[デグレセ\*](#degraisser-gls)
し、必要があれば煮詰める。目の細かい濾し器で濾す。このソースの一部をキュ
イソの上にかけ、残りは別にソーシエールに入れる。ガルニチュールは軽く砂
糖を入れてオレンジとレモンの[ゼスト\*](#zeste-gls)少量ずつを加えた赤ワインで調理した種
抜きドライプラム。



\atoaki{}


#### マルカサンのハム[^7] {#jambon-de-marcassin}

\index{marcassin@marcassin!jambon@Jambon de ---}
\index{jambon marcassin@jambon (marcassin)!marcassin@--- de Marcassin}
\index{まるかさん@マルカサン!はむ@---のハム}
\index{はむまるかさん@ハム（マルカサン）!まるかさんのはむ@マルカサンの---}


<div class="frsubenv">Jambon de marcassin (1-4)</div>

普通の手順で[ブレゼ](#les-braises)する。提供時にプチポワのピュレまたは
ナヴェ、セロリ、マロンのピュレあるいははガルニチュールを添える。ブレゼ
の煮汁に[ソース・ポワヴラード](#sauce-poivrade-pour-gibier)を少し加え、
香辛料をきかせて供する。

北の国々では、「エーグルドゥ」というソースと共に提供する。これはリエーブルと
ラパンにも良く使わる。次の項目でそのレシピを示す。

[^7]: フランス語 jambon （ジョンボン） < jambe （ジョンブ、脚）からわ
    かるように、ハムは骨付き、骨抜きともに\kenten{もも肉}を用いてつく
    られる。スライスなどせずに丸ごと調理されることも多い。


\atoaki{}


#### マルカサンのハム・エーグルドゥ[^6] {#jambon-de-marcassin-a-l-aigre-doux}

\index{marcassin@marcassin!jambon aigre-doux@Jambon de --- à l'aigre-doux}
\index{jambon marcassin@jambon (marcassin)!marcassin aigre-doux@--- de marcassin à l'aigre-douxe}
\index{aigre-doux@aigre-doux (aigre-douce)!jambon marcassin@Jambon de marcassin à l'---}
\index{まるかさん@マルカサン!はむえーくるとう@---のハム・エーグルドゥ}
\index{はむまるかさん@ハム（マルカサン）!まるかさんのはむえーくるとう@マルカサンの---・エーグルドゥ}
\index{えーくるとう@エーグルドゥ（エーグルドゥス）!まるかさんのはむ@マルカサンのハム・---}


<div class="frsubenv">Jambon de marcassin à l’aigre-doux (1-4)</div>

普通の手順で[マルカサンのハム](#jambon-de-marcassin)を[ブレゼ](#les-braises)する。ブレゼの煮汁を用いて[ロー
マ風ソース](#sauce-romaine)（ブラウン系の派生ソース参照）を用意する。このソースにサク
ランボの酢漬け、種を抜いたドライプラム、水溶きショコラを加える。ショコ
ラはソースの仕上がり直前に加えること。

このガルニチュールを兼ねたソースは南イタリアでも同じく使われるが、松の
実、オレンジとセドラ[^4]の皮のコンフィ[^5]を[アシェ\*](#hacher-gls)し
て加える。

[^4]: レモンの一種。

[^5]: 濃いシロップで煮詰めて砂糖漬けにしたもの。

[^6]: aigre （エーグル）> vinaigre （ヴィネーグル）ヴィネガーの語が派
    生していることからもわかるように「酸っぱい」の意。doux （ドゥ）甘
    い、の意。つまり甘酸っぱいソース。ちなみに中国料理の酢豚は porc
    aigre-doux （ポールエーグルドゥ）または porc sauce aigre-douce
    （ポールソールエーグルドゥース）などと呼ばれる。フランスには
    中国系ヴェトナム移民がすくなくないため、中国系ヴェトナム料理は比較
    的ポピュラーなものとなっている。映画『青いパパイヤの香り』や『夏至』
    『ノルウェイの森』で知られるトラン・アン・ユン Trần Anh Hùng （チャ
    ン・アィン・フン、陳英雄）監督もそういった移民のひとり。また小説家で
    フランス文学研究者でもある堀江敏幸の短編「おぱらばん」におい
    てフランスに渡ってきた中国系ヴェトナム人が描かれている。

<!--\atoaki{}-->
</div><!--endRecette-->


