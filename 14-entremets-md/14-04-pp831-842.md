---
author: '五 島　学　訳・注'
pandoc-latex-environment:
  center:
  - center
  flushright:
  - flushright
  frchapenv:
  - frchapenv
  frsecbenv:
  - frsecbenv
  frsecenv:
  - frsecenv
  frsubenv:
  - frsubenv
  main:
  - main
  recette:
  - recette
  pararecette:
  - pararecette
title: 'エスコフィエ『フランス料理の手引き』'
---

<!--EPUB用設定=いったんPandoc.mdに変換後にコメントアウトしてEPUB化すること-->
<!--
\renewcommand{\kenten}[1]{<span class="em-dot">#1</span>}
\renewcommand{\textsc}[1]{<span class="sc">#1</span>}
\renewcommand{\ruby}[2]{<ruby>#1<rt>#2</rt></ruby>}
\setlength{\parindent}{1zw}
-->






## 冷たいアントルメ  {#serie-des-entremets-froids}



### \hspace*{-1zw}冷たいアントルメのソース {#sauces-et-accompagnements-d-entremets-froids}

冷たいアントルメにはソースとして以下のものが添えられる。

1……好みの風味を付けた[クレーム・オングレーズ A](#creme-a-l-anglaise-a)

2……アプリコットやミラベル、レーヌ・クロード、グロゼイユなどのシロップ。
通常、このシロップにベースのフルーツに合わせたリキュールを加えてフルーツ特有の風味を強調させるべきだ。
キルシュとマラスキーノは色々なフルーツに適している。

3……いちご、フランボワーズ、グロゼイユなどの新鮮なフルーツに粉糖少々を加えてピュレにしたものか、
そのピュレに[クレーム・フェッテ](#creme-fouettee-dite-creme-chantilly)を一定の割合で混ぜたもの。

4……好みの風味を付けた[クレーム・シャンティ](#creme-chantilly)

最後に、いくつかのアントルメには下記の[さくらんぼのソース](#sauce-aux-cerises)を添える。

<div class="recette">

#### さくらんぼのソース {#sauce-aux-cerises}

<div class="frsubenv">Sauce aux Cerises</div>

フランボワーズ入りグロゼイユのジュレ 500 g を軽く溶かす。冷やしておいた陶製の器に注ぎ入れる。
コンポートにしておいた新鮮なさくらんぼのコンポート液をジュレと同量と、オレンジ・ソンギーヌの果汁 2 個分、
生姜パウダー少々、鮮やかな色にするために食紅数滴を加える。
キルシュ風味のシロップを軽く温めた中に、前もって浸しておいたさくらんぼの砂糖漬け 125 g 加えて仕上げる。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}ババロワ  {#bavarois}


<div class="frsubenv">BAVAROIS</div>

バヴァロワの注意点。
厨房の色々な料理の中で、型に詰めて冷やすこの名前の付いたクリームは昔「フロマージュ・バヴァロワ」のメニューの
中に記していた。
「フロマージュ」の語句を消してしまうという、いささか美しさに欠け無益な簡略化がされたが、バヴァロワにはフロマージュの
語句の持つ意味[^49bis]が含まれている。

慣用的に使われてはいるが、バヴァロワという名前は非論理的のように思えるし、
より論理的で理にかなっていると思われる「[モスコヴィット](#moscovite)」の名前に代えている。

このような考えから、この本では「バニラ風味やコーヒー風味などのバヴァロワ」の代わりに、
「バニラ風味やコーヒー風味などの[モスコヴィット](#moscovite)」と記している。
（アルファベットの順序でかなり後に記してある[モスコヴィット](#moscovite)を参照）


[^49bis]: フロマージュ（fromage）。英語でチーズcheese、中国語で腐に相当。日本ではカビを生やした
ものや熟成を経たものが一般的で、想像しやすいと思うが、ここでのフロマージュの語句の持つ意味は、中国語の腐
の「液状のものが寄り集まって固形状になった柔らかいもの」の意味に近いと思われる。
例としては「フロマージュ・ド・テット」や「フロマージュ・ブロン」など。

\atoaki{}

### \hspace*{-1zw}ブロン・モンジェ  {#blanc-manger}


ブラン・モンジェをあまり作らなくなってしまったが、前もってきちんと仕込んでいたものを提供できる
素晴らしいアントルメの一つなのに、これは残念だ。

イギリスで供しているブロン・モンジェは一般的に作られているものと全く違うものだが、
以下に示しているようにとてもしっかりとしていて、どちらも同じように素晴らしいアントルメだ。

まずその名前がそう示しているように、論理的に言って、ブロン・モンジェは必ず真っ白でなければいけないが、
長い時間が経ち、この料理名からはその 1 番の特徴が失われてしまった。
白いと言う形容的にも、実際に使われている材料も、区別が明確ではなくなり、
下ごしらえの時に着色されたものにでさえ、全ての物にこの名前が付けられてしまっている。
フランス語とわかりやすい表現という点からの間違いは、たいへん昔のカレームの時代以前のことなので、
目をつむらざるをえない。

<div class="recette">

#### ブロン・モンジェ・フランセーズ  {#blanc-manger-a-la-francaise}


<div class="frsubenv">Blanc-Manger à la Française</div>

材料……スイートアーモンド 500 g とビターアーモンド 4 、 5 個を掃除し、しっかりと白くなるまで冷水にさらす。

出来るだけ細かくすり潰し、水 8 dL を\ruby{濾}{こ}して[^50]大さじ  1 杯ずつ加えて混ぜる。
厚手の布で包んで力強く絞る。

こうして絞り出した、約 7 dL のアーモンドミルクに、角砂糖 200 g を溶かす。
ゼラチン 30 g を軽く温めたシロップに溶かしてアーモンドミルクに加え、モスリンの布でこして、好みの風味を付ける。

型入れ……中央に空洞のある型に油を塗り、[モスコヴィット](#moscovite)と同様に詰める。
氷で冷やして同じようにして型から外す。

###### 【原注】 {#nota-blanc-manger-a-la-francaise}

現在の厨房では、上記のような昔のやり方の代わりに、大さじ数杯の水と脂肪分のとても低いクリームと共に
アーモンドをすり潰してアーモンドミルクを作っている。


[^50]: 不純物等を取り除くためと思われるが、現在の飲料用の水では濾す工程は不要。

\atoaki{}

#### フルーツとリキュールのブロン・モンジェ  {#blancs-mangers-aux-fruits-et-aux-liqueurs}


<div class="frsubenv">Blancs-Mangers aux Fruits et aux Liqueurs</div>

ブロン・モンジェを作ることが出来るように、フルーツを煮詰めてピュレにして、
上記のブロン・モンジェの[アパレイユ\*](#appareil-gls)の半量とフルーツのピュレを同量の割合で混ぜて、
上記と同じ量のゼラチンで固める。

このブロン・モンジェには使うフルーツ、つまり、いちご、フランボワーズ、アプリコット、ももなどの名前を付ける。

リキュールのブロン・モンジェも、[アパレイユ\*](#appareil-gls) 1 L あたり
リキュール $\frac{1}{2}$ dL の割合で同様に作る。
最適なのは、キルシュとマラスキーノ、ラム酒だ。
ショコラ風味とコーヒー風味のブロン・モンジェも同様に作るが、コーヒー風味の方は、
他の風味の物に比べてアーモンドにはあまり合わない。

\atoaki{}

#### ブロン・モンジェ・リュバネ  {#blancs-mangers-rubannes}


<div class="frsubenv">Blancs-Mangers Rubannés</div>

ブロン・モンジェの[アパレイユ\*](#appareil-gls)に様々な色を付けたものと何も加えていないものを、
同じ厚さで均等な層で交互に重ねて型に詰め、[モスコヴィット・リュバネ](#moscovite-rubanne)と同じように作る。

###### 【原注】 {#nota-blancs-mangers-rubannes}

銀製の深皿や上質な磁器の箱、深さのある平らな皿などにブロン・モンジェの[アパレイユ\*](#appareil-gls)を詰めてもいい。
作り方の中で、[アパレイユ\*](#appareil-gls)の固さを決めるゼラチンの量を減らしても良く、そうすると繊細な口当たりになる。

アントルメのブロン・モンジェは型から外さないで作るので、ゼラチンを減らすのは簡単だ。

「Le Cuisinier Parisien」の本の中で、カレームはブロン・モンジェの[アパレイユ\*](#appareil-gls)に、
その $\frac{1}{4}$ の量の非常に新鮮で上質なクリームを軽く泡立てて、[アパレイユ\*](#appareil-gls)に
とろみがつき始めた時に加えることをすすめている。
このひと手間には常に価値があり、これからもそうであるだろう。

\atoaki{}

#### ブロン・モンジェ・イギリス風  {#blanc-manger-a-l-anglaise}


<div class="frsubenv">Blanc-Manger à l’Anglaise</div>

牛乳 1 L に砂糖 125 g を加えて沸かす。
コーンフラワー 125 g に沸かした牛乳を注ぎかけながら混ぜて、冷たい牛乳 2 $\frac{1}{2}$ dL でのばす。

この[アパレイユ\*](#appareil-gls)を泡立て器でしっかり混ぜて\ruby{滑}{なめ}らかにしたら、強火で絶えず混ぜながら数分間加熱する。

[アパレイユ\*](#appareil-gls)を火から外し、好みの香りを付けて、
前もって薄めのシロップで湿らせておいた型に、熱々の[アパレイユ\*](#appareil-gls)を注ぎ入れる。


\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}シャルロット {#charlottes-bis}

<div class="recette">

#### シャルロット・アルルカン[^51] {#charlotte-a-l-arlequine}


<div class="frsubenv">Charlotte à l’Arlequine</div>

シャルロット型の底に無色の丸い紙を敷き、壁面に沿って[ジェノワーズ生地](#pate-a-genoise-ordinaire)を、
白色とピンク色、ピスタチオのフォンダンで[グラセ\*](#glacer-gls)した物を交互に、しっかりと貼り付けながら積み上げる。

別にフラン用のセルクルを、油を塗った紙にのせ、
いちご、チョコレート、ピスタチオ、アプリコットの[モスコヴィット](#moscovite)の
[アパレイユ\*](#appareil-gls)をセルクルの中に詰めて作る。

上記のモスコヴィットを大きめの角切りに切りそろえ、少し\ruby{緩}{ゆる}めの標準的な
[クリームのモスコヴィット](#moscovite-a-la-creme)の中に入れて混ぜる。
全てを混ぜ合わせたクリームをシャルロット型に注ぎ入れそのまま置いておく。

提供直前に型から外し、底に敷いた丸い紙を取って、何も加えていない[ジェノワーズ生地](#pate-a-genoise-ordinaire)の土台にのせる。
フォンダンで[グラセ\*](#glacer-gls)してフルーツの砂糖漬けを飾る。

[^51]: à l'arlequine （アラルルキーヌ）、アルルカン風。
中世フランスの説話に現れるいたずら悪魔エルルカン（Herlequin）、エルカン（Hellequin）
がその語源であるといわれている。
コメディア・デラルテの下男役アルレッキーノのフランス名。
派手な菱形模様のタイツ姿で，小さなマスクをかぶる。
イギリスではHarlequin、ハーレクィンと呼ばれ、道化芝居ハーレクィネードに出てくる若い男の恋人役で、
コランバイン（コロンビーナ）の求婚者。

\atoaki{}

#### シャルロット・カルメン[^52] {#charlotte-carmen}


<div class="frsubenv">Charlotte Carmen</div>

シャルロット型にゴーフレットを貼り付けて、次の[アパレイユ\*](#appareil-gls)を入れる。
トマトのマーマレード 250 g、赤パプリカのマーマレード 125 g、しょうがパウダー少々、
角切りにしたしょうがの砂糖漬け 100 g、レモンジュース 3 個分、32 °Bé[^53]の熱いシロップ 3 dL に
ゼラチン 10 枚を溶かした物。

全て混ぜて、アパレイユにとろみがつき始めたら、クリーム 1 L を泡立てて加える。


[^52]: オペラのカルメン？

[^53]: ボーメ度数ともいう、本来は比重を量るための単位だが、製菓においてはシロップの濃度を示すのに用いられる。

\atoaki{}

#### シャルロット・シャンティ {#charlotte-chantilly}


<div class="frsubenv">Charlotte Chantilly</div>

ゴーフレットで[シャルロット](#charlottes-diverses)を作り、
[フィレの状態に加熱した](#cuisson-du-sucre)アプリコットかあるいは、
[カセの状態に加熱した砂糖](#cuisson-du-sucre)で、
円形の[乾燥焼きした生地](#pate-seche-sucree-pour-differents-usages)の土台の上に固定する。
そのために、シャルロット型の底に円形の[乾燥焼きした生地](#pate-seche-sucree-pour-differents-usages)を置いてから
シャルロットを作り、ゴーフレットと土台がしっかり貼りついたら型を外せばいい。

砂糖とバニラを加えて泡立てたクリームを、ピラミッドの形になるようにシャルロットの側面に沿って絞る。
同じクリームを薄いピンク色にして、コルネ等で表面に点で飾りを描く。

\atoaki{}

#### バケ、パニエ・シャンティ {#baquet-et-panier-chantilly}


<div class="frsubenv">Baquet et Panier Chantilly</div>

バケは、形を整えた[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を
[乾燥焼きした生地](#pate-seche-sucree-pour-differents-usages)に
[カッセの状態に加熱した砂糖](#cuisson-du-sucre)で貼り付けて作る。
貼り付けたビスキュイよりも高さのあるビスキュイの上部に小さな円型の抜き型で穴をあけて、
バケの中心と両側に置き、細いリボン状のチョコレート風味の[パート・ダモンド](#pate-d-amandes-fondante)
を輪っかにしてバケを囲む。

パニエも同様に作るが、ビスキュイの高さを全て同じになるようにして、
輪っかに模した[パート・ダモンド](#pate-d-amandes-fondante)で囲まない。
飴細工の花を飾り付けた飴細工の曲げた取っ手を、[カッセの状態に加熱した砂糖](#cuisson-du-sucre)で土台に固定する。

バケとパニエには[シャルロット・シャンティ](#charlotte-chantilly)と同じクリームを詰め、
同じようにピンク色のクリームで点を描いて仕上げる。

\atoaki{}

#### シャルロット・コリネット[^54] {#charlotte-colinette}


<div class="frsubenv">Charlotte Colinette</div>

シャルロット型の底と側面に薄い紙を敷いて、しっかりと乾かした小さめのメレンゲを全体に貼り付け、
紫色のプラリネを加えたバニラ風味の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)
を一杯に詰める。

シャルロットを型から外し、型の底だった面に形のきれいな紫色のプラリネを
[カラメルにした砂糖](#cuisson-du-sucre)で貼り付けて飾る。

このオントルメは冷やして提供するが、凍らせないこと。

[^54]: 保留中

\atoaki{}

#### シャルロット・モントルイユ[^55] {#charlotte-montreuil}


<div class="frsubenv">Charlotte Montreuil</div>

型の底と側面に[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を貼る。
[モスコヴィット](#moscovite)の[アパレイユ\*](#appareil-gls)を、
[クレーム・オングレーズ](#creme-a-l-anglaise-a) 1 L あたり、もものピュレ 4 dL と泡立てたクリームを標準的な割合で作って型に入れる。

よく熟したももの[エマンセ\*](#emincer-gls)に砂糖を加え、
型に入れた[アパレイユ\*](#appareil-gls)の中に入れる。

[^55]: Montreuil。フランス国内に複数存在する地名。
ラテン語で小さな修道院を意味する語から派生した語。


\atoaki{}

#### シャルロット・ノルマンド {#charlotte-normande}


<div class="frsubenv">Charlotte Normande</div>

シャルロット型に、[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)と同じ形に層状に絞ったメレンゲを敷く。

[シャルロット](#charlotte-de-pommes)用のりんごのマーマレードをしっかりと煮詰めて、すき間なく型一杯に入れる。

型から外し、[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)でシャルロット全体を\ruby{覆}{おお}って同じクリームで飾り付ける。

\atoaki{}

#### シャルロット・オペラ {#charlotte-opera}


<div class="frsubenv">Charlotte Opéra</div>

「パルマーズシュガーウエハース」[^56]を型に敷いて、バニラ風味の[モスコビット](#moscovite)の[アパレイユ\*](#appareil-gls)に、
その $\frac{1}{4}$ の量のとても上質なマロングラッセのピュレと、
マラスキーノで[マセレ\*](#macerer-gls)したフルーツの砂糖漬けの[サルピコン\*](#salpicon-gls)を
加えた[アパレイユ\*](#appareil-gls)を型に入れる。


[^56]: Sugar wafers Palmer’s。Palmer's パルマーズはメーカー名。パルマーズの甘みのついたウェハースを使う。

\atoaki{}

#### シャルロット・プロムビエル[^57] {#charlotte-plombiere}


<div class="frsubenv">Charlotte Plombière</div>

シャルロット型に[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)かゴーフレットを敷く。

提供直前に[グラス・プロムビエル](#glace-plombiere)を型に入れて、型から外してナフキンに盛る。

[^57]: プロムビエルの密約に関係？

\atoaki{}

#### シャルロット・ルネッソンス[^58] {#charlotte-renaissance}


<div class="frsubenv">Charlotte Renaissance</div>

型の底を\ruby{覆}{おお}うように円形の紙を敷き、側面に白色とピンク色に[グラセ\*](#glacer-gls)した長方形の
[ジェノワーズ生地](#pate-a-genoise-ordinaire)を、色付けた面が型に接するように貼る。

皮をむいて[エマンセ\*](#emincer-gls)した生のアプリコットとペシュ、
パイナップルの角切りとフレーズ・デ・ボワを、全て前もって砂糖を加えたキルシュで[マセレ\*](#macerer-gls)しておき、
バニラ風味の[モスコビット](#moscovite)の[アパレイユ\*](#appareil-gls)に加えて、生地を貼った型に入れる。

シャルロットを型から外したら、紙を取って、代わりにパイナップルの 1 番大きい部分を輪切りにしてのせ、フルーツの砂糖漬けで飾る。

[^58]: Renaissance


\atoaki{}

#### シャルロット・リュス[^59] {#charlotte-russe}


<div class="frsubenv">Charlotte Russe</div>

ハート型に切った[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を、
型の底に\ruby{薔薇}{ばら}の形に置き、側面にハート型にした同じビスキュイを立てるように置いて、
ビスキュイ同士をしっかりと貼り付ける。

このシャルロットに詰める[モスコビット](#moscovite)の[アパレイユ\*](#appareil-gls)は、
バニラ風味のクリーム入りやプラリネ入り、またはコーヒー風味、オレンジ風味、チョコレート風味などにしても良い。

また、[モスコビット](#moscovite)の[アパレイユ\*](#appareil-gls)は、
アプリコットやパイナップル、ペシュ、いちごなどのフルーツのピュレを加えたものでも良い。

メニュー名には、オレンジ風味のシャルロット・リュスや、いちごのシャルロット・リュスなどのように、
付けた風味やシャルロットに加えた特徴的な材料の名前を、必ず表記しなくてはいけない。

[^59]: russe、ロシア風の意。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}クリーム {#cremes}

オントルメとして提供するクリームは、2 種類にはっきりと区別される。

1。クレーム・[ポシェ\*](#pocher-gls)、つまり[クレーム・オングレーズ](#creme-a-l-anglaise-a)に変化をつけただけのもの。

2。砂糖を加えて泡立てた生クリームから派生したクリーム、
典型的なものは[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)だ。

クレーム・[ポシェ\*](#pocher-gls)は、専用の鍋かもしくは、銀製か磁器の小さめの深さのある型やまたは、色々な型で作る。
前者のように、鍋や深さのある型で作った場合は必ず加熱した道具に入れたまま提供するのとは対照的に、
後者のように型で作った場合は、しっかりと冷えたら型から外し、それを[クレーム・ロンヴェルセ](#creme-renversee)と呼ぶ。

しかし、この[クレーム・ロンヴェルセ](#creme-renversee)という呼び方は、少しすたれてしまっており、
今はどちらかと言うと「クレーム・ムーレ」という表現を使う。

[クレーム・カラメル](#creme-au-caramel)は、このクレーム・ムーレのパルフェの様式のものだ。

加熱した道具に入れたまま提供するクリームは、[アパレイユ\*](#appareil-gls)の卵の量がそれほど必要ないので、
他のクリームよりも\ruby{滑}{なめ}らかだが、しかしながらイギリス風のカスタードのように
ほとんど家庭で作られるだけだ。少し重要な夕食や昼食にはクレーム・ムーレの方が望ましい。

<div class="recette">


#### クリーム・ムーレ・バニラ風味  {#creme-moulee-a-la-vanille}


<div class="frsubenv">Crème moulée à la Vanille</div>

牛乳 1 L を沸かして砂糖 200 g を溶かし、バニラビーンズ 1 本を入れて約 20 分間[アンフュゼ\*](#infuser-gls)する。

全卵 4 個と卵黄 8 個分を前もって陶製の容器の中で溶いておき、しっかりと混ぜながらこの牛乳を少しずつ注ぎ入れる。
目の細かい\ruby{濾}{こ}し器等でこし、少しの間そのまま置いて、表面に浮いている泡を全て取り除いたら、
このアパレイユをバターを塗った型か、このクリーム用の特別な\ruby{壺}{つぼ}に注ぎ入れる。

型に\ruby{蓋}{ふた}をして中温のオーヴンで\ruby{湯煎}{ゆせん}で[ポシェ\*](#pocher-gls)する。
加熱中に\ruby{湯煎}{ゆせん}の湯が決して沸かないようにすること。
そうしないと温度が高くなり過ぎて、クリームに含まれる空気が膨らみ、最終的に小さな気泡が出来てしまって、
冷やすとクリームに小さな穴がたくさん残り見た目によくない。

要するに、クリームは必ず[ポシェ\*](#pocher-gls)する、言い換えると
\ruby{湯煎}{ゆせん}の湯の温度が 96 ℃ に保たれる火加減で凝固させる。

クリームに火が通ったら、\ruby{湯煎}{ゆせん}から外してそのまま冷やす。

[ポシェ\*](#pocher-gls)した型に入れたまま提供する場合、牛乳 1 L に対して全卵 1 個と卵黄 8 個分の割合で十分だ。
クリームを[ポシェ\*](#pocher-gls)して冷やした後、型を丁寧に\ruby{拭}{ふ}いてナフキンにのせる。

もしクリームを型から外す場合は、型を丁寧に裏返して提供用の皿にのせ、数分間そのまま置いてから型を外す。

クリーム・ムーレ、または容器入りのクリームの風味付けはオントルメ用に使うものであればなんでも良いが、
その中でも特に適しているのは、バニラ、アーモンドミルク、アーモンドとヘーゼルナッツのプラリネ、コーヒー、チョコレートなどである。
フルーツの風味は、香りのとても強いエッセンスのようなものでなければあまり合わない。

\atoaki{}

#### クリーム・ムーレ・キャラメル風味  {#creme-moulee-au-caramel}


<div class="frsubenv">Crème moulée au Caramel</div>

砂糖を黄金色のキャラメル状に加熱して、型の底と側面に薄く流して貼り付け、
[クリーム・ムーレ・バニラ風味](#creme-moulee-a-la-vanille)の[アパレイユ\*](#appareil-gls)を入れる。

上記に書いてあるのと同様に、[ポシェ\*](#pocher-gls)して型から外す。

\atoaki{}

#### クリーム・ムーレ・フロランタン  {#creme-moulee-a-la-florentine}


<div class="frsubenv">Crème moulée à la Florentine</div>

[クリーム・キャラメル・プラリネ](#creme-au-caramel-pralinee)の[アパレイユ\*](#appareil-gls)を用意し、
型に入れて[ポシェ\*](#pocher-gls)する。

クリームがしっかり冷えたら、型から外して提供用の皿にのせ、
キルシュ風味の[クリーム・シャンティ](#creme-chantilly)で飾り付けて表面に
ピスタチオの[アシェ\*](#hacher-gls)をふる。


\atoaki{}

#### クリーム・ムーレ・オペラ  {#creme-moulee-opera}


<div class="frsubenv">Crème moulée Opéra</div>

[クリーム・プラリネ](#creme-pralinee)の[アパレイユ\*](#appareil-gls)をふちに飾りの付いた型に入れて[ポシェ\*](#pocher-gls)する。

クリームを型から外したら、クリームの中央に、紫色のプラリネ風味の[クリーム・シャンティ](#creme-chantilly)をドーム状に盛る。

キルシュ風味のシロップで[マセレ\*](#macerer-gls)しておいた大きめのいちごをドーム状のクリームの周りに 1 周置いて、糸状の飴を上にかぶせる。

\atoaki{}

#### クリーム・ムーレ・ヴィエノワーズ  {#creme-moulee-a-la-viennoise}


<div class="frsubenv">Crème moulée à la Viennoise</div>

これはクリーム・キャラメルの一つで、その中でも、黄金色のキャラメル状に[加熱した砂糖](#cuisson-du-sucre)を、
型に流して貼り付ける代わりに、温かい牛乳に溶かしたものだ。
[クリーム・バニラ](#creme-a-la-vanille)と全く同じように作る。


\atoaki{}

### \hspace*{-1zw}泡立てたクリームがベースの冷たいクリーム {#cremes-froides-a-base-de-creme-fouettee}


#### クリーム・シャンティ  {#creme-chantilly}


<div class="frsubenv">Crème Chantilly</div>

とても新鮮で十分に濃い生クリームを使い、泡立て器ですくえる固さになるまで泡立てる。

クリーム 1 L あたり粉糖 250 g を加え、バニラかフルーツのエッセンスで香り付けする。

何に使う場合でも、出来る限りこのクリームは、使う直前に仕上げる。

\atoaki{}

#### クリーム・シャンティ・フルーツ入り  {#creme-chantilly-aux-fruits}


<div class="frsubenv">Crème Chantilly aux fruits</div>

このクリームは、使うフルーツのピュレ $\frac{1}{3}$ 量と
[クリーム・シャンティ](#creme-chantilly) $\frac{2}{3}$ 量の割合で作る。
加える砂糖の量と風味はフルーツの種類に合わせて変える。

このクリームは、同じクリームやフルーツで飾り付けて、オントルメの付け合わせとして、またはグラスに盛って提供する。

[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)を別添で提供する。

\atoaki{}

#### クリーム・カプリス  {#creme-caprice}


<div class="frsubenv">Crème Caprice</div>

[クレーム・シャンティ](#creme-chantilly) $\frac{1}{2}$ L に、
その $\frac{1}{4}$ 量の [メレンゲ](#meringue-ordinaire)を大きめに砕いて加える。

マドレーヌ型に白い紙を敷いて[アパレイユ\*](#appareil-gls)を入れ、密閉して、多めに塩を入れた氷の中に
2 時間入れる。

型から外したら紙を取って、いちごかフランボワーズの果汁を少量加えてピンク色にした[クレーム・シャンティ](#creme-chantilly)を、
ギザ模様の口金を付けた絞り袋に入れて、飾り付ける。

\atoaki{}

#### ブリズ・プランタン  {#brise-de-printemps}


<div class="frsubenv">Brise de Printemps</div>

[クレーム・シャンティ](#creme-chantilly)をスミレで風味付けし、
軽く冷やして、クリスタルの小さめのグラスにスプーンで盛る。紫色のプラランを数個のせる。


\atoaki{}




</div> <!--endRecette-->

### \hspace*{-1zw}ジュレ {#gelees}

作り方の点から、ジュレは 2 種類ある。

1。 ワインとリキュールのジュレ。

2。 フルーツのジュレ。

どちらのジュレもベースはまったく同じで、指定の量の水に溶かしたゼラチンだ。

このゼラチンは仔牛の足を茹でてとった濃いもので、これは最良のものだが、作り方が最も複雑でもある。
普通は、下記の割合で、市販のゼラチンを単に使えば良い。

仔牛の足のゼラチン……大きめの仔牛の足を用意し、血抜きして[ブロンシール\*](#blanchir-gls)する。
仔牛の足 1 個あたり水の量を 1 L の割合で茹でる。
出来る限り完全に[エキュメ\*](#ecumer-gls)もして、\ruby{蓋}{ふた}をしたままごく弱火で 7 時間茹でる。

茹で終わったら、茹で汁をこして徹底的に[デグレセ\*](#degraisser-gls)する。
少量を氷の上で冷やしてゼラチンの固さを確認し、必要であれば、必要量の水をこして加えて調節し、氷にのせて確認し直す。

このゼラチン 1 L あたり、砂糖 250 g、シナモン少々、オレンジとレモン各 $\frac{1}{2}$ 個の[ゼスト\*](#zeste-gls)、
オレンジとレモン各 1 個分の果汁を加える。

下記のようにして[クラリフィエ\*](#clarifier-gls)する。

ゼラチンベースのジュレ……水 1 L あたりゼラチン 35 g を溶かす。砂糖 250 g、レモン $\frac{1}{2}$ 個分と
オレンジ 1 個分の[ゼスト\*](#zeste-gls)とジュースを加える。沸かして、火から外して 10 分間そのまま置いておく。

[クラリフィエ\*](#clarifier-gls)……とても清潔な鍋で卵白 1 個分と白ポルト酒グラス $\frac{1}{2}$ 杯を
よく混ぜる。濃いめのシロップを少しずつ、混ぜながら卵白に注ぎ入れる。

鍋を火にかけて、沸騰し始めるまで絶えず混ぜ続ける。沸騰したら、鍋を火の横に外し、
ジュレがわずかに沸いている状態で、 $\frac{1}{4}$ 時間加熱する。

 $\frac{1}{4}$ 時間経ったら、[クラリフィエ\*](#clarifier-gls)は終わりで、とても清潔な陶製の容器に
 \ruby{円錐}{えんすい}型のこし器をおいて、ジュレをこす。
こし始めのジュレが\ruby{濁}{にご}っている時は、完全に澄み切るまで別の新しいこし器でこし直さなければいけない。
風味付けする前に、 $\frac{3}{4}$ 程度[^101]冷ましておく。

風味付け……市販の一般的なゼラチンか仔牛の足どちらを使うにせよ、上記のように作ったものは、単に
濃度のあるシロップでしかなく、それに加える何かしらの風味付けが、オントルメ用のジュレに特徴をつける。

ジュレを特徴付ける材料は、リキュール、上質なワイン、またはフルーツの果汁で、ジュレを作る時の水の量を、
あとで加えるこれらの風味付けの液体の量の分減らす。

要するに、リキュールで風味付けするジュレ 1 L には、ジュレ 9 dL と好みのリキュール、
例えばキルシュ、マラスキーノ、ラム酒、アニゼット酒などを 1 dL 使う。

上質なワインのジュレ……例えば、シャンパーニュ 、マデラ酒、シェリー、マルサラ酒などを使って、
ジュレ 7 dL と好みのワイン 3 dL で 1 L に仕上げる。

フルーツのジュレに関しては、使うフルーツの種類によって作り方は変わる。

いちご、フランボワーズ、グロゼイユ、さくらんぼ、コケモモなどの赤いフルーツのジュレを作る時は、
よく熟れているこれらのフルーツを裏ごして、一つに混ぜ合わせる。
そのフルーツのピュレは程度の差はあっても粘りがあるので、ピュレ 500 g あたり水 3 dL を加える。
そうしたら、ピュレをこして赤い果汁を取り、同量のゼラチンを足す。
従って、果汁が十分に固まるように、ゼラチンは倍ほど濃くなければいけない。

フルーツに粘りがあり過ぎる時は、裏ごしたピュレをそのまま数時間発酵させておき、発酵によって澄んだ果汁の
不純物をこして取り除く。

水分の多いフルーツ、例えば、ぶどう、オレンジ、レモン、マンダリンオレンジなども、同じように作る。
これをこして果汁をとる作業は簡単だが、ぶどうは別で、この果汁は決して発酵させるべきではない。

フルーツがあまり熟れてない時は、[クラリフィエ\*](#clarifier-gls)する前のジュレに果汁を加えてもいい。
この作り方は余計な酸味を除くことが出来る点が良い。
このフルーツの果汁の割合は、赤い果実のものとほぼ同じ割合にする。

アプリコット、ペシュ、ネクタリン、プルーンなどの、種があるフルーツの時は、
フルーツのジュレの付け合わせと同じように使うが、風味付けのベースに使う事はあまりない。
使う場合は下記のようにする。
沸騰した湯に落として皮を前もって\ruby{剥}{む}いておき、あとでジュレにするシロップで[ポシェ\*](#pocher-gls)して、
そのシロップの中に入れたまま冷やしておく。

このジュレを[クラリフィエ\*](#clarifier-gls)して $\frac{3}{4}$ 程度冷ました後、
使ったフルーツの風味を強調するのにキルシュかマラスキーノ酒を少量加えるべきだ。

[^101]: 原文、Laisser refroidir aux trois quarts、完全に冷やして固まる前の、
ある程度まで冷ました状態にすること、と思われる。

\atoaki{}


### \hspace*{-1zw}ジュレに加える物と添え物 {#garnitures-et-accompagnement-des-gelees}

大抵ジュレはそのまま提供する。
しかし、色々な種類のフルーツを加熱してコンポートにし、様々な色のフルーツをジュレの中に対称的に置いて添えることも、
時にはある。

このように作ったジュレは、フルーツ・スエドワーズという名前をつける。

###### 【原注】 {#nota-garnitures-et-accompagnement-des-gelees}

ジュレに加えた風味付けによっては、メニューにジュレ・マセドワーヌという名前でも表記することがある。

ジュレ・リュバネ……これは、色々な風味と色を付けたジュレを、型に同じ厚さで交互に層状に重ねる。

ジュレ・ロシア風……これは標準的なジュレを、固まり始めるまで氷の上で泡立てるように混ぜて作る。
固まり始めたら素早く型の中に入れて固める。

このジュレを2、3種類使う時は、違う風味と色を付け、型に入れる時に、美しい\ruby{柄}{がら}の
大理石模様になるようにジュレを上手く混ぜる。

ジュレ・モスコヴィット……標準的な密閉できる型で作って、さらにリボン状のバターで\ruby{蓋}{ふた}と容器の\ruby{隙間}{すきま}をうめる。
その後、型を氷漬けにしてしっかりと冷やし固める。

ジュレの型の周りの氷に塩をふって温度を下げることで、ジュレの表面が薄く凍ったような美しい見た目になる。
但しジュレが固まって、その薄い層が出来たらすぐに型から取り出す必要がある。
氷の中に入れる時間が長過ぎると、ジュレは食べられない氷の塊になり、さらに濁ってしまう。

###### 【原注】 {#nota-les-gelees-a-la-moscovite}

最近の作り方では、ジュレの盛り付け方と提供の仕方がとても簡素化している。
今では、ジュレは専用のグラスや銀製の深さのある皿に盛り、型は一般的に使わない。
時々、ジュレを入れる皿の底にフルーツのコンポートやマセドワーヌを置いて、その上にジュレを入れて覆うこともあり、
その皿のまま提供する場合は、ゼラチンの量を半分に減らしても良く、そうすると、ジュレがやわらかく、とても繊細に仕上がる。
切って提供するフルーツのジュレやその他の全てのジュレは、今ではりんごの果汁が良い状態で保存されているものが
あるので、りんごのジュレをベースとしている。


\atoaki{}

<div class="recette">

#### ジュレ・ミス・アリエット  {#gelee-miss-helyett}


<div class="frsubenv">Gelée Miss Helyett</div>

キルシュ風味のジュレにその $\frac{1}{3}$ の量の新鮮なフランボワーズの果汁をこして加える。
\ruby{縁}{ふち}に模様の付いた広さのある型に注ぎ入れて、そのまま固める。

型から外して砂糖入りの生地の土台の上にのせ、ジュレの中央に[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)を置く。
[ポシェ\*](#pocher-gls)して冷ましておいた新鮮なフルーツで、ジュレと土台の生地（ジュレの\ruby{縁}{ふち}の周り）を飾る。

プラランを刻んで散りばめる。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}モスコヴィット（旧バヴァロワ）

モスコヴィットは 2 通りの作り方がある。

1。クリーム入りのもの。

2。フルーツ入りのもの。

<div class="recette">

#### モスコヴィット・クリーム入り {#moscovite-a-la-creme}


<div class="frsubenv">Moscovite à la Crème</div>

クリーム入りの[アパレイユ\*](#appareil-gls)……粉糖 500 g と卵黄 16 個分を鍋に入れてしっかり混ぜ、
前もってバニラビーンズ 1 本で[アンフュゼ\*](#infuser-gls)しておいた牛乳 1 L を沸かして加えてのばす。
ゼラチン 25 g を冷水で戻して加える。アパレイユを弱火にかけて、沸かさないようにしながらスプーンにしっかりと張り付くようになるまで加熱する。

シノワでこして、\ruby{釉薬}{うわぐすり}を塗ってある陶製の型に入れ、たまに混ぜながら冷やして、
とろみがつき始めたら、[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly) 1 L と粉糖 100 g、バニラシュガー 25 g を加える。

\atoaki{}

#### モスコヴィット・フルーツ入り {#moscovite-aux-fruits}


<div class="frsubenv">Moscovite aux Fruits</div>

フルーツ入りのアパレイユ……フルーツのピュレ 5 dL を、30 °Bé のシロップ 5 dL でのばす。
レモン果汁 3 個分と溶かして布でこしておいたゼラチン 30 g 、
[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly) $\frac{1}{2}$ L を加える。

モスコヴィット・フルーツ入りの[アパレイユ\*](#appareil-gls)には、ピュレで使ったフルーツをそのまま加えても良い。
いちごやフランボワーズ、グロゼイユなどを使う場合は、それらのフルーツは生で加える。
洋梨、ペシュ、アプリコットなど、果肉がしっかりとしたフルーツを使う場合はシロップで[ポシェ\*](#pocher-gls)しておく。

\atoaki{}

</div> <!--endRecette-->

### \hspace*{-1zw}モスコヴィットの型入れと盛り付け

モスコヴィットは一般的に、スイートアーモンドの油を薄く塗った、真ん中に穴の空いた型に入れて作る。
型いっぱいに入れたら、[アパレイユ\*](#appareil-gls)に白い紙で\ruby{蓋}{ふた}をしたあとに、砕いた氷で型の周りを\ruby{覆}{おお}う。

提供直前に、型をぬるま湯にごく短時間つけて水気を拭き、型を裏返して提供用の皿にのせ、
折りたたんだナプキンで\ruby{蓋}{ふた}をするか、またはそのまま提供する。

型に油を塗る代わりに、[カラメル状に加熱した砂糖](#cuisson-du-sucre)で薄く\ruby{覆}{おお}っても良く、
そうすると見た目にも良く、味わいも素晴らしくなる。

非常におすすめの作り方がもう一つあり、それは、モスコヴィットを銀製の深さのある型か皿に入れて氷で冷やす方法だ。
この場合、菓子は型から外さないので、[アパレイユ\*](#appareil-gls)の固さをとても\ruby{緩}{ゆる}くすることが出来て、繊細な食感になる。

最後の方法で容器に入れる場合は、新鮮な[フルーツのマセドワーヌ](#macedoine-de-fruits-rafraichis)
か[コンポート](#compotes-simples)を付け合わせで提供することもある。
ただし、このフルーツの付け合わせは、むしろ[冷たいプディング](#puddings-froids)の方が合うし、
そもそもモスコヴィットと[冷たいプディング](#puddings-froids)はとても似ている。

最後に、モスコヴィットを容器に入れても入れなくても、提供する直前に、絞り袋かコルネを使って、
白色かピンク色の[クレーム・シャンティ](#creme-fouettee-dite-creme-chantilly)で飾る。

\atoaki{}

<div class="recette">

#### モスコヴィット・クレルモン  {#moscovite-clermont}


<div class="frsubenv">Moscovite Clermont</div>

バニラ風味のモスコヴィットの[アパレイユ\*](#appareil-gls) 1 L あたり[マロンのピュレ](#puree-de-marrons) 200 g を加える。

型から外したら、大きめの[マロングラッセ](#marrons-braises-et-glaces)を周りに 1 周並べる。


\atoaki{}


#### モスコヴィット・ディプロマット  {#moscovite-diplomate}


<div class="frsubenv">Moscovite Diplomate</div>

タンバル型の内側一面にバニラ風味のモスコヴィットの[アパレイユ\*](#appareil-gls)を塗る。

チョコレート風味といちご風味のモスコヴィットの[アパレイユ\*](#appareil-gls)を、交互に均一な厚さの層状にしながら型一杯に詰める。


\atoaki{}


#### モスコヴィット・ミクィーン  {#moscovite-my-queen}


<div class="frsubenv">Moscovite My Queen</div>

モスコヴィットの型の内側を、砂糖少々と溶かしたゼラチンを加えた生クリームで\ruby{覆}{おお}う。
そうしたら、キルシュで[マセレ\*](#macerer-gls)しておいた大きめのいちごを加えた、
いちごのピュレ入りのモスコヴィットの[アパレイユ\*](#appareil-gls)を型一杯に入れる。

菓子を型から外したら、砂糖とキルシュで同じように[マセレ\*](#macerer-gls)しておいた大きめのいちごを、周りに一周並べる。


\atoaki{}


#### モスコヴィット・ノルマンド風  {#moscovite-a-la-normande}


<div class="frsubenv">Moscovite à la Normande</div>

ラム酒風味の上質なりんごのマーマレード 500 g あたり、角切りのりんご 100 g をバターで加熱して加え、
目的に合わせた量のとても新鮮なクリームを半立てにして混ぜて仕上げたものを、
フルーツのモスコヴィットの[アパレイユ\*](#appareil-gls)に入れる。

菓子を型から外したら、バニラ風味のシロップで茹でておいた、くし切りのりんごを底面の周りに置く。



\atoaki{}


#### モスコヴィット・ルリジユーズ  {#moscovite-a-la-religieuse}


<div class="frsubenv">Moscovite à la Religieuse</div>

しっかりと十分に固まるくらいに、シロップに溶かしたチョコレートで型の内側を\ruby{覆}{おお}う。
[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)の代わりに、
そのままの生クリームで作ったバニラ風味のモスコヴィットの[アパレイユ\*](#appareil-gls)を型に入れる。


\atoaki{}


#### モスコヴィット・リュバネ  {#moscovite-rubanne}


<div class="frsubenv">Moscovite Rubanné</div>

この種類のモスコヴィットは、色々な風味と色を付けた[アパレイユ\*](#appareil-gls)を、交互に層になるように型に詰める。
見た目の決まりは特になく、使うモスコヴィットの[アパレイユ\*](#appareil-gls)の種類は色々と変えて良い。


\atoaki{}


#### その他のモスコヴィット・クリーム入り  {#moscovites-devers-a-la-creme}


<div class="frsubenv">Moscovites divers à la Crème</div>

アーモンド風味、アニゼット風味、ヘーゼルナッツ風味、コーヒー風味、チョコレート風味、キルシュ風味、
生のクルミ風味、オレンジ風味、スミレ風味などのモスコヴィットの[アパレイユ\*](#appareil-gls)は、
[モスコヴィット・クリーム入り](#moscovite-a-la-creme)に書いてあるように作って、風味付けを変えるけだ。

\atoaki{}


#### その他のモスコヴィット・フルーツ入り {#moscovites-divers-aux-fruits}


<div class="frsubenv">Moscovites divers aux fruits</div>

[モスコヴィット・フルーツ入り](#moscovite-aux-fruits)に書いてあるように作れば、パイナップル風味、アプリコット風味、
いちご風味、フランボワーズ風味、メロン風味などのモスコヴィットを作ることが出来る。


\atoaki{}


</div> <!--endRecette-->

### \hspace*{-1zw}冷たいプディング {#puddings-froids}

モスコヴィットと冷たいプディングはよく似ていて、大抵の場合、この 2 つの作り方の基本は同じだ。

それらを区別するのに最もはっきりしているのは、モスコヴィットは一般的にソースも付け合わせも添えずに提供するのに
対して、プディングはどちらか、または両方とも添えて提供する。

プディングのソースは、この章の冒頭に書いてある。
ガルニチュールは[コンポート](#compotes-simples)にしたフルーツを別添で提供するか、
プディングの[アパレイユ\*](#appareil-gls)にフルーツの砂糖漬けを加える。


<div class="recette">

#### プディング・アランベルク {#pudding-d-aremberg}


<div class="frsubenv">Pudding d’Aremberg</div>

キルシュ風味のモスコヴィットの[アパレイユ\*](#appareil-gls)を作って、熟れた生の洋梨のピュレを加える。
冷やしておいたマドレーヌ型の底に、[ポシェ\*](#pocher-gls)した白色とピンク色のくし切りの洋梨と、
小さく切りそろえたフルーツの砂糖漬けをきれいに並べる。
薄く切ってキルシュを染み込ませた[ビスキュイ・パンシュ](#pate-a-biscuit-punch)を、型の壁面に貼る。

[アパレイユ\*](#appareil-gls)を型に流し入れてそのまま置いておく。


\atoaki{}


#### プディング・ボエミオン {#pudding-bohemienne}


<div class="frsubenv">Pudding Bohémienne</div>

とても小さい[クレープ](#crepes)を焼き、フルーツの砂糖漬けの[サルピコン\*](#salpicon-gls)と
ぬるま湯で戻しておいたドライレーズンを、りんごと洋梨の濃いめのマーマレードで[リエ\*](#lier-gls)して、
[クレープ](#crepes)にのせる。
[クレープ](#crepes)を丸か長方形に折りたたみ、バターを塗った縁に飾りのある型に並べて、
最後に全卵の量を少し増やしたクリーム入りのプディングの[アパレイユ\*](#appareil-gls)を型一杯に入れる。
湯\ruby{煎}{せん}で[ポシェ\*](#pocher-gls)する。

型に入れたまま冷まし、型から外したら、好みの風味を付けた[サバイヨン](#sabayon)をプディングの表面に塗る。


\atoaki{}


#### プディング・クレルモン {#pudding-clermont}


<div class="frsubenv">Pudding Clermont</div>

ラム酒風味のモスコヴィットの[アパレイユ\*](#appareil-gls)を用意する。その $\frac{1}{4}$ 量の
[マロングラッセ](#marrons-braises-et-glaces)のピュレと、[アパレイユ\*](#appareil-gls) 1 L あたり、
同じマロンのかけらを 150 g 加える。


\atoaki{}


#### プディング・ディプロマット {#pudding-diplomate}


<div class="frsubenv">Pudding Diplomate</div>

高さのある縁に飾りの付いた型に油を塗って、フルーツの砂糖漬けを小さく切りそろえたものを、
型の底にきれいに敷き詰める。

バニラ風味のモスコヴィットの[アパレイユ\*](#appareil-gls)と、
キルシュを染み込ませた[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)
を交互に層状に重ねながら型に入れる。
ビスキュイの各層の上に、ぬるめのシロップで戻しておいたコリント産とスミルヌ産のレーズンをちりばめ、
所々に[アプリコットのコンフィチュール](#confitures-d-abricots)を大さじ数杯おく。

冷たい場所に置くか、氷で冷やして提供直前に型から外す。

\atoaki{}


#### プディング・ディプロマット・フルーツ入り {#pudding-diplomate-aux-fruits}


<div class="frsubenv">Pudding Diplomate aux fruits</div>

上記のプディングに、熟れた洋梨、ペッシュ、アプリコットなどの皮を\ruby{剥}{む}いて薄く切り、
砂糖を加えたリキュールで前もって[マセレ\*](#macerer-gls)しておいたフルーツの層を足して、同じように作る。

プディングを型から外したら、使うフルーツのうちの 1 種類のよく冷えた[コンポート](#compotes-simples)か、
[コンポート](#compotes-simples)を混ぜたものを、プディングの底の周りに置く。


\atoaki{}


#### プディング・マラコフ {#pudding-malakoff}


<div class="frsubenv">Pudding Malakoff</div>

用意する物。
1……[ゼラチン入りのクレーム・オングレーズ](#creme-a-l-anglaise-b) 1 L あたり、
とても新鮮なクレーム・ドゥーブル 5 dL を加える。
2……[りんごのシャルロット](#charlotte-de-pommes)用の洋梨とりんごのマーマレードと、
ぬるめのシロップで戻しておいたコリント産とスミルヌ産のレーズン、新鮮なアーモンドの細切り、
オレンジの皮の砂糖漬けの角切り、固くなった薄切りのビスキュイかリキュールを染み込ませておいた
[ビスキュイ・キュイエール](#pate-a-biscuits-a-la-cuiller)。

シャルロット型に油を塗り、底に 1 cm の厚さにクレームを流し込んで、たっぷりとマーマレードを塗った
ビスキュイをこのクリームの上に一面におく。
ビスキュイの上に、レーズンとアーモンド、オレンジの皮を散りばめる。
クレームを一面に\ruby{覆}{おお}うように塗ったあとに、ビスキュイをもう 1 度重ねて、
その後もまた同じように重ねて詰める。

休ませておき、提供直前に型から外して提供用の皿にのせ、キルシュ風味の[サバイヨン](#sabayon)で
プディングを\ruby{覆}{おお}う。

\atoaki{}

#### プディング・ネッスルロッド {#pudding-nesselrode}


<div class="frsubenv">Pudding Nesselrode</div>

なめらかな[マロンのピュレ](#puree-de-marrons) 250 g と、
ぬるま湯で戻しておいたスミルヌ産とコリント産レーズン、オレンジの皮とさくらんぼの砂糖漬けの角切りを、
それぞれほぼ同量ずつ混ぜたもの 125 g を、何も加えていない[クレーム・オングレーズ](#creme-a-l-anglaise-a) 1 L に加える。
上記のフルーツは、前もって砂糖を加えたマデラ酒に漬けておくこと。

このアパレイユに、モスコヴィット用の[アパレイユ\*](#appareil-gls)と同じ割合で
[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)を加え、マラスキーノ酒で風味付けする。

シャルロット型の底と側面に白い紙を貼り付け、[アパレイユ\*](#appareil-gls)を注ぎ入れて密封し、
帯状のバターで蓋と型の\ruby{隙間}{すきま}を囲い、塩を加えた氷でしっかりと固めてそのまま置いておく。

提供直前に、プディングを型から外してナプキンに載せ、紙を取って、チョコレートで[グラセ\*](#glacer-gls)した
大きめの[マロン・グラッセ](#marrons-braises-et-glaces)か、
丸く絞った[マロンのピュレ](#puree-de-marrons)を、底の周りに一周並べる。

###### 【原注】 {#nota-pudding-nesselrode}

アイスクリームマシンで[クレーム・オングレーズ](#creme-a-l-anglaise-a)を固めても良く、
クレームが少し固まったら、[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)を混ぜて、
その後型に詰める。

\atoaki{}


#### プディングまたはクリーム・レーヌ・デ・フェ[^102] {#pudding-ou-creme-reine-des-fees}


<div class="frsubenv">Pudding ou Créme Reine des Fées</div>

卵白 4 個分と[加熱した砂糖でメレンゲ・イタリエンヌ](#meringus-itakienne-au-sucre-cuit)を作り、
同量の[カリンのジュレ](#gelee-de-coings)を加えて、フルーツの砂糖漬けの角切りをキルシュに浸けて、
よく水分を切っておいたものを加えて仕上げる。
このメレンゲを大きめのボタン状に紙の上に絞る。

この紙を入れるのに十分な広さの鍋などに、砂糖 1 kg とキルシュ 1 $\frac{1}{2}$ dL を加えた水 4 L を入れて沸かす。
紙をこのシロップの中に滑らせるように入れて、ボタン状のメレンゲが紙から簡単に\ruby{剥}{は}がれるようになったら
紙を取って、そのままメレンゲを[ポシェ\*](#pocher-gls)し、布の上に置いて水気を切ってそのまま冷ます。

別で、モスコヴィットの[アパレイユ\*](#appareil-gls)を 2 種類、バニラ風味の白い物とキュラーソー風味のピンク色の物を用意する。

これらのアパレイユには、[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)を、
標準的なモスコヴィットに加える 2 倍の量を加える必要があり、ゼラチンの量は半分に減らすこと。

冷やして軽く油を塗ったマドレーヌ型に、これらの[アパレイユ\*](#appareil-gls)を同じ厚さの層状に交互に、
間にメレンゲを挟みながら、重ね入れる。
紙を全体にかぶせて\ruby{蓋}{ふた}をし、2 時間しっかりと冷やし固める。

提供直前に、型から外してナプキンに載せる。

[^102]: Reine des Fées、妖精の女王の意。

\atoaki{}


#### プディング・リツィオ {#pudding-rizzio}


<div class="frsubenv">Pudding Rizzio</div>

この作り方は[プディング・ディプロマット](#pudding-diplomate)と同じだ。
中に入れるものは、ロソーリオ[^103]とキルシュに浸しておいた砕いたやわらかいマカロンと、
シロップで[ポシェ\*](#pocher-gls)した新鮮なフルーツを入れる。


[^103]: rosolio、イタリア語で「太陽のしずく」の意、バラの香りの付いたリキュール。

\atoaki{}


#### 米のクリームのプディング {#pudding-de-riz-a-la-creme-froide}


<div class="frsubenv">Pudding de Riz à la Crème</div>

このプディングはそのままか、フルーツの冷たい[コンポート](#compotes-simples)を添えて提供する。
作り方は[温かい米のクリームのプディング](#pudding-de-riz-a-la-creme)と同じだが、
[リエ\*](#lier-gls)した後に、オーヴンで[グラセ\*](#glacer-gls)する代わりに、
[アパレイユ\*](#appareil-gls)に[泡立てたクリーム](#creme-fouettee-dite-creme-chantilly)少々と、
米 1 リーヴル[^104]あたり、溶かしたゼラチン 4 枚分、やわらかいマカロン 125 g 大きめに砕いてキルシュに浸しておいたものを加えてそのまま冷やす。
提供用の皿に盛って、安定させるのに、表面に粉糖をふって赤くなるまで熱した鉄の棒で模様を付ける。

[^104]: livre、重量の単位。1 リーヴルは 500 g

\atoaki{}


#### スムールのプディング・クリーム入り {#pudding-de-semoule-a-la-creme}


<div class="frsubenv">Pudding de Semoule à la Crème</div>

上記の[米のクリームのプディング](#pudding-de-riz-a-la-creme-froide)に書いてあるのと同じように作る。

\atoaki{}

</div> <!--endRecette-->
